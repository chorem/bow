import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    authenticated: false,
    user: {},
    query: {info: {}, tags:[[],[]], result: []}
  },
  getters: {},
  mutations: {
    query(state, payload) {
      state.query = payload
    },
    user(state, payload) {
      state.user = payload
    },
    authenticated(state, payload) {
      state.authenticated = payload
    }
  },
  actions: {}
})
