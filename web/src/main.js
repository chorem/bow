// Make sure to register before importing any components
import './class-component-hooks'

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import FetchHelper from '@/utils/FetchHelper.js'
import StoreHelper from './utils/Store'
import VueDOMPurifyHTML from 'vue-dompurify-html'
import vuexI18n from 'vuex-i18n'

window.BACKEND_URL = process.env.VUE_APP_BACKEND_URL
window.FRONTEND_URL = process.env.BASE_URL

// register utilities function on window, Vue and Vue instance
if (typeof window !== 'undefined') {
  window.$fetch = FetchHelper
  window.$storage = StoreHelper
}

Vue.use(VueDOMPurifyHTML)
Vue.use(vuexI18n.plugin, store)

Vue.$fetch = FetchHelper
Vue.$storage = StoreHelper

Vue.config.productionTip = false
Vue.prototype.$fetch = FetchHelper
Vue.prototype.$storage = StoreHelper

Vue.mixin({
  methods: {
    saveUser: function(newUser) {
      this.$storage.set('bow-user', newUser)
    },
    go: function(fullQuery) {
      console.log('go query', fullQuery)

      // keep only non default params, to not pollute url
      let query = {}
      fullQuery.first && (query.first = fullQuery.first)
      fullQuery.tags && (query.tags = fullQuery.tags)
      fullQuery.fulltext && (query.fulltext = fullQuery.fulltext)
      fullQuery.query && (query.query = fullQuery.query)
      fullQuery.orderby != 'creationdate' && (query.orderby = fullQuery.orderby)
      fullQuery.orderasc && (query.orderasc = fullQuery.orderasc)

      console.log('go params', query)
      this.$router.push({ name: 'Home', query })
    },
    changeOrderBy: function(orderby) {
      let query = { ...this.currentQuery }
      if (query.orderby !== orderby) {
        query.orderby = orderby
        this.go(query)
      }
    },
    changeOrderAsc: function(orderasc) {
      let query = { ...this.currentQuery }
      if (query.orderasc !== orderasc) {
        query.orderasc = orderasc
        this.go(query)
      }
    },
    changeFulltextAndGo: function(fulltext) {
      let query = { ...this.currentQuery }
      if (fulltext !== query.fulltext) {
        query.first = 0
        query.fulltext = fulltext
        this.go(query)
      }
    },
    changeTagsAndGo: function(tags) {
      let query = { ...this.currentQuery }
      if (tags.length !== query.tags.length || !tags.reduce((a, b) => a && query.tags.includes(b), true)) {
        query.first = 0
        query.tags = tags
        this.go(query)
      }
    },
    goFirst: function() {
      let query = { ...this.currentQuery }
      if (query.first != 0) {
        query.first = 0
        this.go(query)
      }
    },
    goNext: function() {
      let query = { ...this.currentQuery }
      if (query.first + this.user.maxresult < this.queryInfo.total) {
        query.first = query.first + this.user.maxresult
        this.go(query)
      }
    },
    goPrev: function() {
      let c = this.currentQuery
      let query = { ...c }
      query.first = query.first - this.user.maxresult
      if (query.first < 0) {
        query.first = 0
      }
      if (query.first != c.first) {
        this.go(query)
      }
    },
    goLast: function() {
      let c = this.currentQuery
      let query = { ...c }
      query.first = this.queryInfo.total - this.user.maxresult
      if (query.first < 0) {
        query.first = 0
      }
      if (query.first != c.first) {
        this.go(query)
      }
    },
    addTagsAndGo: function(tags) {
      let query = { ...this.currentQuery }
      query.tags = query.tags ? [...query.tags] : []
      tags.forEach((t) => {
        let index = query.tags.indexOf(t)
        if (index === -1) {
          query.tags.push(t)
        } else {
          query.tags.splice(index, 1)
        }
      })
      this.go(query)
    }
  },
  computed: {
    authenticated: function() {
      return this.$store.state.authenticated
    },
    user: function() {
      return this.$store.state.user
    },
    currentQuery: function() {
      // faut-il mettre aussi: id ? uri ?
      let i = this.queryInfo
      return {
        first: i.first,
        tags: i.tags,
        fulltext: i.fulltext,
        query: i.query,
        orderby: i.orderby,
        orderasc: i.orderasc
      }
    },
    queryInfo: function() {
      return this.$store.state.query.info || {}
    },
    queryTags: function() {
      let tags = this.$store.state.query.tags || [[], []]
      return tags[0]
    },
    queryTagsCount: function() {
      let tags = this.$store.state.query.tags || [[], []]
      return tags[1]
    },
    queryResult: function() {
      return this.$store.state.query.result || []
    }
  }
})

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
