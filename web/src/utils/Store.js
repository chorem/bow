// contient des pairs {key, callback}
let listeners = []

function fireEvent(key, value) {
  console.log('-------------> storage change', key, value)
  listeners.filter(l => l.key === key).forEach(l => {
    l.callback.call(l.me, value, key)
  })
}

/*
Default store/read to/from localStorage
If data not found on localStorage, looking for value in cookie
*/
let StoreHelper = {

  get(key) {
    let value = localStorage.getItem(key) || this.getCookie(key)
    return value === undefined ? undefined : JSON.parse(value)
  },

  set(key, value) {
    // value must not be undefined (error when JSON.parse during get)
    localStorage.setItem(key, JSON.stringify(value || null))
    fireEvent(key, value)
  },

  delete(key) {
    localStorage.removeItem(key)
    fireEvent(key, undefined)
  },

  has(key) {
    let value = localStorage.getItem(key) || this.getCookie(key)
    return value !== undefined && value !== null
  },

  getRaw(key) {
    return localStorage.getItem(key)
  },

  setRaw(key, value) {
    localStorage.setItem(key, value)
  },

  getCookie(key) {
    let ekey = encodeURIComponent(key)
    let value = document.cookie && document.cookie.split(';')
      .map(s => s.trim())
      .filter(s => s.startsWith(ekey + '='))
      .map(s => decodeURIComponent(s.replace(/[^=]+=(.*)/, '$1')))[0]
       
    return value
  },

  addListener(key, callback, me) {
    listeners.push({key, callback, me})
  },

  removeEventListener(key, callback) {
    listeners = listeners.filter(e => e.key !== key && e.callback !== callback)
  }
}

export default StoreHelper
