import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Preferences from '../views/Preferences.vue'
import Home from '../views/Home.vue'
import BookmarkEdit from '../views/BookmarkEdit.vue'
import PageHistory from '../views/PageHistory.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/preferences',
    name: 'Preferences',
    component: Preferences,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      requiresAuth: true
    },
    props: (route) => ({
      id: route.query.id,
      tags: route.query.tags,
      fulltext: route.query.fulltext,
      query: route.query.query,
      orderby: route.query.orderby,
      orderasc: route.query.orderasc,
      first: route.query.first
    })
  },
  {
    path: '/edit/:id',
    name: 'Edit',
    component: BookmarkEdit,
    meta: {
      requiresAuth: true
    },
    props: (route) => ({
      id: route.params.id,
      uri: route.query.uri,
      description: route.query.description,
      tags: route.query.tags,
      privatealias: route.query.privatealias,
      publicalias: route.query.publicalias,
      lang: route.query.lang
    })
  },
  {
    path: '/history/:id',
    name: 'History',
    component: PageHistory,
    meta: {
      requiresAuth: true
    },
    props: (route) => ({
      id: route.params.id,
      date: route.query.date
    })
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth) && !window.$storage.getCookie('bow-token')) {
    next({
      path: '/login',
      params: { nextUrl: to.fullPath }
    })
  } else {
    next()
  }
})

export default router
