== to start back

# Postgresql database must be start
export DATABASE_URL="postgres://dbuser:xxxxxxxx@localhost:5432/bow"
export BOW_PUBLIC_URL="http://localhost:8000"

go run cmd/bow/main.go

== to start front

cd web
yarn serve


== TODO
- verifier que les contraintes n'unicitées sont bien respectées (alias public)
- faire une action 'visit' qui charge juste la page demander mais en plus la met dans l'historique des actions faites
- utiliser ActionHistory pour retenir les pages visités depuis bow ou non (action 'visit')
- utiliser https://github.com/mozilla/readability et https://github.com/cure53/DOMPurify pour récupérer l'artible de la page et non pas toute la page pour la stocker lorsqu'on la met en bookmark
  (voir si c'est un service externe ou directement dans bow (plutot solution externe)
- utiliser https://github.com/sensepost/gowitness pour generer les screenshots (en faire un service externe dockeriser appelé par bow)
  gowitness --debug --disable-db --resolution-x 596 --resolution-y 842 server
  convert /tmp/cl3.png -scale 65x91 /tmp/cl3mini.png
  