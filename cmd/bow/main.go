package main

import (
	"log"
	"os"

	"gitlab.chorem.org/chorem/bow/pkg/http"
	"gitlab.chorem.org/chorem/bow/pkg/repository"
	"gitlab.chorem.org/chorem/bow/pkg/utils"
)

func main() {
	databaseURL := os.Getenv("DATABASE_URL")
	secretKey := os.Getenv("SECRET_KEY")
	bowPublicURL := os.Getenv("BOW_PUBLIC_URL")
	migrateDir := os.Getenv("DATABASE_MIGRATION_DIR")

	if secretKey == "" {
		// for dev only
		secretKey = "AZERTYUIOPQSDFGHJKLMWXCVBN"
	}

	if migrateDir == "" {
		migrateDir = "migrate"
	}
 
	log.Println("Init database")
	repository.Init(databaseURL, migrateDir, true)
	utils.JwtInit([]byte(secretKey))

	// u := model.BowUser{ID: utils.GenUUID(), Login: "bpoussin", Emails: []string{"toto@pouss.in"}, Password: "toto"}

	// err := repository.CreateUser(u)
	// if err != nil {
	// 	log.Fatalln(err)
	// }

	// s, err := repository.UserJSON("toto@pouss.in")
	// if err != nil {
	// 	log.Fatalln(err)
	// }
	// log.Println("User: ", s)

	addr := ":8000"
	log.Println("Start web server", addr)
	http.Start(bowPublicURL, addr)
}
