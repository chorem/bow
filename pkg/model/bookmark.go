package model

import (
	"time"
)

/*
Bookmark lien a sauvegarder
*/
type Bookmark struct {
	ID                 string             `json:"id,omitempty"`
	Owner              string             `json:"owner,omitempty"`
	URI                string             `json:"uri,omitempty"`
	Description        string             `json:"description,omitempty"`
	Tags               []string           `json:"tags,omitempty"`
	CreationDate       time.Time          `json:"creationdate,omitempty"`
	UpdateDate         time.Time          `json:"updatedate,omitempty"`
	// poiteur pour qu'il puisse etre null
	ImportDate         *time.Time          `json:"importdate,omitempty"`
	PrivateAlias       []string           `json:"privatealias,omitempty"`
	PublicAlias        []string           `json:"publicalias,omitempty"`
	// poiteur pour qu'il puisse etre null
	AuthenticationInfo *AuthenticationInfo `json:"authenticationinfo,omitempty"`
	Favicon            []byte             `json:"favicon,omitempty"`
	Screenshot         []byte             `json:"screenshot,omitempty"`
	Visit              int64              `json:"visit"` // on veut zero dans la base, donc ne pas mettre omitempty
	Lang               string             `json:"lang,omitempty"`
}
