package model

import (
	"time"
)

/*
ActionHistory un historique d'action
*/
type ActionHistory struct {
	ID           string    `json:"id,omitempty"`
	Owner        string    `json:"owner,omitempty"`
	CreationDate time.Time `json:"creationdate,omitempty"`
	Action       Action    `json:"action,omitempty"`
	Request      string    `json:"request,omitempty"`
	Lang         string    `json:"lang,omitempty"`
}
