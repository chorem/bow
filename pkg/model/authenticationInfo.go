package model

/*
AuthenticationInfo contient les informations pour
s'authentifier sur une site

	DomainComponent: le nombre de composant du domain a prendre (default 2, ex: codelutin.com)
	Salt: chaine ajouter en prefix du master password avant le hash
	Suffix: chaine ajouter en suffix du hash final genere (le password genere peut donc etre plus long que maxLength)
*/
type AuthenticationInfo struct {
	Description     string `json:"description,omitempty"`
	Login           string `json:"login,omitempty"`
	DomainComponent byte   `json:"domaincomponent,omitempty"`
	MaxLength       byte   `json:"maxlength,omitempty"`
	AllowedChar     string `json:"allowedchar,omitempty"`
	DisallowedChar  string `json:"disallowedchar,omitempty"`
	Salt            string `json:"salt,omitempty"`
	Suffix          string `json:"suffix,omitempty"`
}
