package model

import (
	"time"
)

/*
BowUser un utilisateur de bow avec ses emails et ses preferences

    Tokens: en cle le token, en valeur l'app qui l'utilise (pour quelle raison se token existe
    Emails: en cle l'email, en valeur une chaine random (uuid) qui permet la validation
*/
type BowUser struct {
	ID                   string              `json:"id,omitempty"`
	CreationDate         time.Time           `json:"creationdate,omitempty"`
	UpdateDate           time.Time           `json:"updatedate,omitempty"`
	Admin                bool                `json:"admin,omitempty"`
	Login                string              `json:"login,omitempty"`
	Password             string              `json:"password,omitempty"`
	Tokens               []Token             `json:"tokens,omitempty"`
	Emails               []string            `json:"emails,omitempty"`
	UnconfirmedEmails    []UnconfirmedEmails `json:"unconfirmedemails,omitempty"`
	AuthenticationInfo   *AuthenticationInfo `json:"authenticationinfo,omitempty"`
	AutoScreenshot       bool                `json:"autoscreenshot"`
	AutoFavicon          bool                `json:"autofavicon"`
	CollectActionHistory bool                `json:"collectactionhistory"`
	MaxTagInCloud        int16               `json:"maxtagincloud,omitempty"`
	MaxResult            int16               `json:"maxresult,omitempty"`
	Actions              []Action            `json:"actions,omitempty"`
	Lang                 string              `json:"lang,omitempty"`
}

// Token un token d'authentification
type Token struct {
	Name       string     `json:"name,omitempty"`
	Token      string     `json:"token,omitempty"`
	Expiration *time.Time `json:"expiration,omitempty"`
}

// UnconfirmedEmails un email pas encore confirme
type UnconfirmedEmails struct {
	Email        string    `json:"email,omitempty"`
	Token        string    `json:"token,omitempty"`
	CreationDate time.Time `json:"creationdate,omitempty"`
}

/*
Action une action est un raccourci (prefix) utilise dans la
bare d'adresse
*/
type Action struct {
	Prefix  string `json:"prefix,omitempty"`
	Action  string `json:"action,omitempty"`
	Suggest string `json:"suggest,omitempty"`
}
