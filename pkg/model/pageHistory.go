package model

import (
	"time"
)

/*
PageHistory un historique de page
*/
type PageHistory struct {
	ID           string    `json:"id,omitempty"`
	CreationDate time.Time `json:"creationdate,omitempty"`
	URI          string    `json:"uri,omitempty"`
	Content      string    `json:"content,omitempty"`
	Lang         string    `json:"lang,omitempty"`
}
