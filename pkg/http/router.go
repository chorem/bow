package http

import (
	"context"
	// ajout du monitoring de go (memoire, allocation, ...)
	// endpoint /debug/vars
	"expvar"
	"fmt"
	"io"
	"log"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.chorem.org/chorem/bow/pkg/constant"
	"gitlab.chorem.org/chorem/bow/pkg/model"
	"gitlab.chorem.org/chorem/bow/pkg/repository"
	"gitlab.chorem.org/chorem/bow/pkg/utils"

	"github.com/gorilla/mux"
)

// BowPublicURL url public 
var BowPublicURL string

var stats = utils.NewStats()

/*
Start web server
*/
func Start(bowPublicURL string, addr string) {
	initMimeType()
	BowPublicURL = bowPublicURL

	router := mux.NewRouter()
	router.Use(logAll)
	// router.Use(mux.CORSMethodMiddleware(router))
	router.Use(cors)
	router.Use(authentication)
	// router.Handle("/debug/{*}", http.DefaultServeMux)

	// router.HandleFunc("/", handler404)

	s := router.PathPrefix("/api/v1").Subrouter()
	s.HandleFunc("/system/liveness", isAlive).Methods(http.MethodGet, http.MethodOptions)
	s.Handle("/system/metrics", expvar.Handler()).Methods(http.MethodGet, http.MethodOptions)
	s.HandleFunc("/system/stats", getStats).Methods(http.MethodGet, http.MethodOptions)
	s.HandleFunc("/users", createUser).Methods(http.MethodPost, http.MethodOptions)
	s.HandleFunc("/users/auth", createAuth).Methods(http.MethodPost, http.MethodOptions)

	u := s.PathPrefix("/users/{id}").Subrouter()
	u.Use(convertCurrentToID)
	u.HandleFunc("", getUser).Methods(http.MethodGet, http.MethodOptions)
	u.HandleFunc("", deleteUser).Methods(http.MethodDelete, http.MethodOptions)
	u.HandleFunc("/auth", createAuth).Methods(http.MethodPost, http.MethodOptions)
	u.HandleFunc("/auth", deleteAuth).Methods(http.MethodDelete, http.MethodOptions)
	u.HandleFunc("/password", updateUserPassword).Methods(http.MethodPut, http.MethodOptions)
	u.HandleFunc("/token", addUserToken).Methods(http.MethodPost, http.MethodOptions)
	u.HandleFunc("/unconfirmedemails", addUserUnconfirmedEmail).Methods(http.MethodPost, http.MethodOptions)
	u.HandleFunc("/unconfirmedemails/{token}", confirmUserEmail).Methods(http.MethodGet, http.MethodOptions)
	u.HandleFunc("/authenticationinfo", updateUserAuthenticationInfo).Methods(http.MethodPut, http.MethodOptions)
	u.HandleFunc("/actions", updateUserActions).Methods(http.MethodPut, http.MethodOptions)
	u.HandleFunc("/autoscreenshot", updateUserAutoScreenshot).Methods(http.MethodPut, http.MethodOptions)
	u.HandleFunc("/autofavicon", updateUserAutoFavicon).Methods(http.MethodPut, http.MethodOptions)
	u.HandleFunc("/collectactionhistory", updateUserCollectActionHistory).Methods(http.MethodPut, http.MethodOptions)
	u.HandleFunc("/maxtagincloud", updateUserMaxTagInCloud).Methods(http.MethodPut, http.MethodOptions)
	u.HandleFunc("/maxresult", updateUserMaxResult).Methods(http.MethodPut, http.MethodOptions)
	u.HandleFunc("/lang", updateLang).Methods(http.MethodPut, http.MethodOptions)

	s.HandleFunc("/bookmarks", getBookmarks).Methods(http.MethodGet, http.MethodOptions)
	s.HandleFunc("/bookmarks", addBookmark).Methods(http.MethodPost, http.MethodOptions)
	s.HandleFunc("/bookmarks/tags", getTags).Methods(http.MethodGet, http.MethodOptions)
	s.HandleFunc("/bookmarks/tags/{oldName}/{newName}", renameTag).Methods(http.MethodPut, http.MethodOptions)
	s.HandleFunc("/bookmarks/tags/{oldName}", renameTag).Methods(http.MethodDelete, http.MethodOptions)
	s.HandleFunc("/bookmarks/{id}", getBookmark).Methods(http.MethodGet, http.MethodOptions)
	s.HandleFunc("/bookmarks/{id}", deleteBookmark).Methods(http.MethodDelete, http.MethodOptions)
	s.HandleFunc("/bookmarks/{id}", updateBookmark).Methods(http.MethodPut, http.MethodOptions)
	s.HandleFunc("/bookmarks/{id}/visit", addOneVisit).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)
	s.HandleFunc("/bookmarks/{id}/authenticationinfo", updateBookmarkAuthenticationInfo).Methods(http.MethodPut, http.MethodOptions)
	s.HandleFunc("/bookmarks/{id}/history", getPageHistory).Methods(http.MethodGet, http.MethodOptions)

	s.HandleFunc("/opensearch", doActions).Methods(http.MethodGet, http.MethodPost, http.MethodOptions).Queries(constant.Action, "{ask}")
	s.HandleFunc("/opensearch", doSuggestion).Methods(http.MethodGet, http.MethodPost, http.MethodOptions).Queries(constant.Suggestion, "{ask}")
	s.HandleFunc("/opensearch", opensearchFile).Methods(http.MethodGet, http.MethodOptions)

	spa := spaHandler{staticPath: "web", indexPath: "index.html"}
	router.PathPrefix("/").Handler(spa)

	srv := &http.Server{
		Handler: router,
		Addr:    addr,
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}

func initMimeType() {
	mime.AddExtensionType(".json", "application/json")
}

func handler404(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "404 page not found", 404)
}

func logAll(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s", r.RemoteAddr, r.Method, r.URL.Path)

		query := r.URL.Query()
		debug := query.Get("bow_debug")
		if debug != "" {
			constant.Debug = debug == "true"
		}

		if constant.Debug {
			log.Println("logAll", r)
		}
		next.ServeHTTP(w, r)
	})
}

func cors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Timing-Allow-Origin", r.Header.Get("Origin"))
		w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		if r.Method == http.MethodOptions {
			w.Header().Set("Access-Control-Allow-Methods", r.Header.Get("Access-Control-Request-Method"))
			if r.Header.Get("Access-Control-Request-Headers") != "" {
				w.Header().Set("Access-Control-Allow-Headers", strings.Join(r.Header.Values("Access-Control-Request-Headers"), ","))
			}
			return
		}

		next.ServeHTTP(w, r)
	})
}

func withoutAuthenticationEndpoint(r *http.Request) bool {
	result := !strings.HasPrefix(r.URL.Path, "/api") || // no auth for SPA (html, css, ...)
		strings.HasSuffix(r.URL.Path, "/auth") || // no auth to create/delete auth
		strings.HasSuffix(r.URL.Path, "/system/liveness") || // no auth to test if server is up
		strings.HasSuffix(r.URL.Path, "/users") // no auth for creation of user
	return result
}

func needAdminEndpoint(r *http.Request) bool {
	result := strings.HasSuffix(r.URL.Path, "/system/")
	return result
}

func convertCurrentToID(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]
		if id == "current" {
			currentUser := r.Context().Value(constant.User).(model.BowUser)
			mux.Vars(r)["id"] = currentUser.ID
		}

		next.ServeHTTP(w, r)
	})
}

func authentication(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if withoutAuthenticationEndpoint(r) {
			next.ServeHTTP(w, r)
			return
		}

		authStat := stats.StartStat("authentication", r.URL.String())

		canBeAppToken := false

		// 1 as query param
		query := r.URL.Query()
		token := query.Get(constant.Token)

		// 2 as bow header
		if token == "" {
			token = r.Header.Get(constant.TokenHeader)
		} else {
			canBeAppToken = true
		}

		// 3 as Authorization header
		if token == "" {
			tokenBearer := r.Header.Get("Authorization")
			if tokenBearer != "" {
				splitted := strings.Split(tokenBearer, " ") //The token normally comes in format `Bearer {token-body}`, we check if the retrieved token matched this requirement
				if len(splitted) != 2 {
					w.WriteHeader(http.StatusForbidden)
					w.Header().Add("Content-Type", "application/json")
					io.WriteString(w, `{"message": "Invalid/Malformed auth token header Authorization"}`)
					return
				}
				token = splitted[1]
			}
		}

		// 4 as cookie
		if token == "" {
			cookie, err := r.Cookie(constant.Token)
			if err == nil {
				token = cookie.Value
			}
		}

		var user model.BowUser
		var err error

		if token != "" {
			// try as jwt token first
			user, err = utils.JwtVerify(token)
			if err != nil && canBeAppToken {
				// try as application token
				user, err = repository.UserFromToken(token, constant.AuthFields...)
				if err != nil {
					utils.Throw(w, utils.NewHTTPError500(err, constant.Nobody))
					return
				}
			} else if err != nil {
				utils.Throw(w, err)
				return
			} else {
				// on force le rechargement du user
				log.Println("Try to load user", user.ID)
				tmp, err := repository.User(user, user.ID, constant.AuthFields...)
				if err != nil {
					utils.Throw(w, err)
					return
				}
				user = tmp
			}
		} else {
			utils.Throw(w, utils.NewHTTPError("Need authentication", user, 401))
			return
		}

		log.Printf("User is '%s'\n", user.ID)

		ctx := context.WithValue(r.Context(), constant.User, user)
		r = r.WithContext(ctx)
		authStat.Stop()

		if needAdminEndpoint(r) && !user.Admin {
			utils.Throw(w, utils.NewHTTPError("Need admin user", user, 403))
			return
		}

		routeName := getRouteName(r)
		restStat := stats.StartStat(fmt.Sprintf("rest(%s %v)", r.Method, routeName), r.URL.String())
		next.ServeHTTP(w, r)
		restStat.Stop()
	})

}

func getRouteName(r *http.Request) string {
	currentRoute := mux.CurrentRoute(r)
	result := currentRoute.GetName()
	if result == "" {
		result, _ = currentRoute.GetPathTemplate()
		if result == "" {
			result, _ = currentRoute.GetPathRegexp()
		}
	}
	return result
}

// spaHandler implements the http.Handler interface, so we can use it
// to respond to HTTP requests. The path to the static directory and
// path to the index file within that static directory are used to
// serve the SPA in the given static directory.
type spaHandler struct {
	staticPath string
	indexPath  string
}

// ServeHTTP inspects the URL path to locate a file within the static dir
// on the SPA handler. If a file is found, it will be served. If not, the
// file located at the index path on the SPA handler will be served. This
// is suitable behavior for serving an SPA (single page application).
func (h spaHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// get the absolute path to prevent directory traversal
	path, err := filepath.Abs(r.URL.Path)
	if err != nil {
		// if we failed to get the absolute path respond with a 400 bad request
		// and stop
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// prepend the path with the path to the static directory
	path = filepath.Join(h.staticPath, path)

	// check whether a file exists at the given path
	_, err = os.Stat(path)
	if os.IsNotExist(err) {
		if strings.HasPrefix(r.URL.Path, "/i18n/") {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}
		// file does not exist, serve index.html
		http.ServeFile(w, r, filepath.Join(h.staticPath, h.indexPath))
		return
	} else if err != nil {
		// if we got an error (that wasn't that the file doesn't exist) stating the
		// file, return a 500 internal server error and stop
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// otherwise, use http.FileServer to serve the static dir
	http.FileServer(http.Dir(h.staticPath)).ServeHTTP(w, r)
}
