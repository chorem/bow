package http

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.chorem.org/chorem/bow/pkg/constant"
	"gitlab.chorem.org/chorem/bow/pkg/model"
	"gitlab.chorem.org/chorem/bow/pkg/repository"
	"gitlab.chorem.org/chorem/bow/pkg/utils"
)

func opensearchFile(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	token, err := utils.JwtGenerate(currentUser)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError500(err, currentUser))
		return
	}

	log.Println("URL ", r.URL.EscapedPath(), "path", r.URL.Path, "raw", r.URL.Hostname(), "end")
	self := BowPublicURL + r.URL.EscapedPath()
	fileContent := fmt.Sprintf(`<?xml version="1.0" encoding="utf-8"?>
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/" xmlns:moz="http://www.mozilla.org/2006/browser/search/">
    <ShortName>Bow</ShortName>
    <Description>Search with Bow</Description>
    <InputEncoding>UTF-8</InputEncoding>
    <LongName>Bow Search</LongName>
    <Image height="16" width="16">data:image/x-icon;base64,R0lGODlhEAAQAMMCAAAAAP97AP////+9hP/nxv+cQsbGxoSEhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAUAAAgALAAAAAAQABAAAARHEIE5kbWUhr0rFmA2ccAFhJIJdNJwlqnZEaUXYwB97Xe+wryUbxUA8nwY1lEnM7ZqsiKuAKxKTwYn7Ic9OIXFiTeIu353lQgAOw==</Image>
    <Url type="text/html" method="get" template="%[1]s">
        <Param name="action" value="{searchTerms}"/>
    </Url>
    <Url type="application/x-suggestions+json" template="%[1]s">
        <Param name="suggestion" value="{searchTerms}"/>
        <Param name="bow-token" value="%[2]s"/>
	</Url>
	<Url type="application/opensearchdescription+xml" rel="self" template="%[1]s" />
    <moz:SearchForm>%[3]s</moz:SearchForm>
</OpenSearchDescription>`, self, token, BowPublicURL)

	w.Header().Add("Content-Type", "application/opensearchdescription+xml")
	io.WriteString(w, fileContent)
}

func getOrEmpty(args []string, i int) string {
	result := ""
	if len(args) > i {
		result = args[i]
	}

	return result
}

func prependPrefix(prefix string, result string) string {
	if prefix == "" {
		return result
	}
	result = strings.ReplaceAll(result, "[\"", "[\""+prefix)
	result = strings.ReplaceAll(result, ",\"", ",\""+prefix)
	result = strings.ReplaceAll(result, ", \"", ", \""+prefix)
	return result
}

var actions = map[string]func(w http.ResponseWriter, r *http.Request, currentUser model.BowUser, prefix string, ask string){
	constant.Save: func(w http.ResponseWriter, r *http.Request, currentUser model.BowUser, prefix string, ask string) {
		args := strings.Split(ask, "|")
		uri := getOrEmpty(args, 0)
		description := getOrEmpty(args, 1)
		tags := getOrEmpty(args, 2)
		privatealias := getOrEmpty(args, 3)
		publicalias := getOrEmpty(args, 4)
		lang := getOrEmpty(args, 5)

		url := fmt.Sprintf("%s/edit/new?uri=%s&description=%s&tags=%s&privatealias=%s&publicalias=%s&lang=%s", BowPublicURL, uri, description, tags, privatealias, publicalias, lang)
		http.Redirect(w, r, url, http.StatusFound)
	},
	constant.RedirectAlias: func(w http.ResponseWriter, r *http.Request, currentUser model.BowUser, prefix string, ask string) {
		uri, err := repository.URIFromAlias(currentUser, ask)
		if err != nil {
			utils.Throw(w, utils.NewHTTPError500(err, currentUser))
			return
		}
		http.Redirect(w, r, uri, http.StatusFound)
	},
	constant.SuggestionAlias: func(w http.ResponseWriter, r *http.Request, currentUser model.BowUser, prefix string, ask string) {
		// ["la saisie actuelle",["tableau de suggestion"]]
		result, err := repository.URIsFromAliasJSON(currentUser, ask)
		if err != nil {
			utils.Throw(w, utils.NewHTTPError500(err, currentUser))
			return
		}
		s := fmt.Sprintf("[\"%s%s\",%s]", prefix, ask, result)
		log.Println("Suggest", s)
		io.WriteString(w, s)
	},
	constant.SearchFulltext: func(w http.ResponseWriter, r *http.Request, currentUser model.BowUser, prefix string, ask string) {
		url := fmt.Sprintf("%s/?fulltext=%s", BowPublicURL,ask)
		http.Redirect(w, r, url, http.StatusFound)
	},
	constant.SuggestionFulltext: func(w http.ResponseWriter, r *http.Request, currentUser model.BowUser, prefix string, ask string) {
		// renvoyer les couple uri/description? pour l'affichage en suggest
		//http.Redirect(w, r, uri, http.StatusFound)
	},
	constant.SearchTag: func(w http.ResponseWriter, r *http.Request, currentUser model.BowUser, prefix string, ask string) {
		url := fmt.Sprintf("%s/?tags=%s", BowPublicURL, ask)
		http.Redirect(w, r, url, http.StatusFound)
	},
	constant.SuggestionTag: func(w http.ResponseWriter, r *http.Request, currentUser model.BowUser, prefix string, ask string) {
		// ["la saisie actuelle",["tableau de suggestion"],["tableau de description des suggetions"], ["tableau d'url si la suggesion est selection"]]
		result, err := repository.TagsJSON(currentUser, ask, true)
		if err != nil {
			utils.Throw(w, utils.NewHTTPError500(err, currentUser))
			return
		}
		s := fmt.Sprintf("[\"%s\",%s", ask, result[1:])
		s = prependPrefix(prefix, s)
		log.Println("Suggest", s)
		io.WriteString(w, s)
	}}

func getAction(currentUser model.BowUser, ask string) model.Action {
	// les actions ne sont plus dans le cookie (sinon cookie trop gros >4ko)
	// donc si on a pas les actions, on les charges (on a les actions si c'est un token qui est utilisé)
	actions := currentUser.Actions
	if len(actions) == 0 {
		tmp, err := repository.User(currentUser, currentUser.ID, "actions")
		if err == nil {
			actions = tmp.Actions
		} else {
			log.Println("Can't get action for", currentUser.ID, err)
		}
	}

	var result model.Action
	for _, a := range actions {
		prefix := a.Prefix
		log.Printf("getAction %v == %v %v (%v && %v || %v) \n", ask, result, a, strings.HasPrefix(ask, prefix), result.Prefix == "", len(prefix) > len(result.Prefix))
		if strings.HasPrefix(ask, prefix) && (result.Prefix == "" || len(prefix) > len(result.Prefix)) {
			result = a
			log.Printf("=====> %v", result)
		}
	}
	return result
}

func doActions(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	ask := mux.Vars(r)["ask"]
	actionTodo := getAction(currentUser, ask)
	todo := actionTodo.Action
	ask = ask[len(actionTodo.Prefix):]

	if currentUser.CollectActionHistory {
		repository.CreateActionHistory(currentUser, actionTodo, ask)
	}

	toCall := actions[todo]
	if toCall != nil {
		// c'est une action system (add, alias, search by tag, ...)
		toCall(w, r, currentUser, actionTodo.Prefix, ask)
	} else {
		// c'est une action ecrite par l'utilisateur (une url)
		uri := strings.ReplaceAll(todo, "{searchTerms}", ask)
		http.Redirect(w, r, uri, http.StatusFound)
	}
}

func doSuggestion(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	ask := mux.Vars(r)["ask"]
	actionTodo := getAction(currentUser, ask)
	todo := actionTodo.Suggest
	ask = ask[len(actionTodo.Prefix):]

	toCall := actions[todo]
	if toCall != nil {
		toCall(w, r, currentUser, actionTodo.Prefix, ask)
	} else {
		uri := strings.ReplaceAll(todo, "{searchTerms}", url.QueryEscape(ask))
		log.Println("get suggestion from ", uri)
		resp, err := http.Get(uri)
		if err != nil {
			utils.Throw(w, utils.NewHTTPError500(err, currentUser))
			return
		}

		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			utils.Throw(w, utils.NewHTTPError500(err, currentUser))
			return
		}

		result := string(body)
		if resp.StatusCode >= 400 {
			utils.Throw(w, utils.NewHTTPError(fmt.Sprintf("Can't retrieve suggestion: %s", result), currentUser, resp.StatusCode))
			return
		}

		result = prependPrefix(actionTodo.Prefix, result)
		log.Println("Suggest", result)
		io.WriteString(w, result)
	}

}
