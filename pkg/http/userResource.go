package http

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.chorem.org/chorem/bow/pkg/constant"
	"gitlab.chorem.org/chorem/bow/pkg/model"
	"gitlab.chorem.org/chorem/bow/pkg/repository"
	"gitlab.chorem.org/chorem/bow/pkg/utils"
)

/*
deleteAuth remove cookie authentication
*/
func deleteAuth(w http.ResponseWriter, r *http.Request) {
	cookie := http.Cookie{Name: constant.Token, Value: "", Path: "/", HttpOnly: false, Expires: time.Unix(0, 0)}
	http.SetCookie(w, &cookie)
}

/*
CreateAuth create JWT token and set header, cookie, body
body: {"token": "xxxx"}
*/
func createAuth(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	var data map[string]string
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError400(err, constant.Nobody))
		return
	}

	if id = checkLogin(id, data["email"], data["password"]); id != "" {
		log.Println("create token", data["email"], id)

		pseudoUser := model.BowUser{ID: id}

		userJSON, err := repository.UserJSON(pseudoUser, id, constant.AuthFields...)
		if err != nil {
			utils.Throw(w, utils.NewHTTPError500(err, pseudoUser))
			return
		}

		var user model.BowUser
		err = json.Unmarshal([]byte(userJSON), &user)
		if err != nil {
			utils.Throw(w, utils.NewHTTPError500(err, pseudoUser))
			return
		}

		token, err := utils.JwtGenerate(user) // si on ajoute plus d'info sensible dans le token, il faudra le chiffrer
		if err != nil {
			utils.Throw(w, err)
			return
		}
		w.Header().Add(constant.TokenHeader, token)

		expiration := time.Now().AddDate(10, 0, 0) // le cookie est valide 10ans :)
		cookieToken := http.Cookie{Name: constant.Token, Value: token, Path: "/", Expires: expiration, HttpOnly: false}
		http.SetCookie(w, &cookieToken)

		w.Header().Add("Content-Type", "application/json")
		io.WriteString(w, userJSON)
	} else {
		utils.Throw(w, utils.NewHTTPError("Bad id or password", constant.Nobody, 403))
	}
}

func checkLogin(id string, email string, password string) string {
	if id != "" && repository.CheckUserPasswordForID(id, password) {
		return id
	}

	result, err := repository.CheckUserPasswordForEmail(email, password)
	if err != nil {
		log.Println("Erreur DB", err)
		return ""
	}

	return result
}

/*
GetUser return all information on user (info, config, auth)
*/
func getUser(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]
	json, err := repository.UserJSON(currentUser, id)
	if err != nil {
		utils.Throw(w, err)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	io.WriteString(w, json)
}

/*
createUser
body: {"login": "toto", "password": "xxxx"}
return: {"id": "[uuid]"}
*/
func createUser(w http.ResponseWriter, r *http.Request) {
	var data map[string]string
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	log.Println("createUser", data["login"])

	id, err := repository.CreateUser(data["login"], data["password"])
	if err != nil {
		utils.Throw(w, err)
		return
	}

	json.NewEncoder(w).Encode(map[string]string{"id": id})
}

func deleteUser(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]
	log.Println("deleteUser", id)

	err := repository.DeleteUser(currentUser, id)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}
}

/*
updateUserPassword
body: {"password": "xxxx", "oldPassword": "yyyy"}
*/
func updateUserPassword(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]
	log.Println("updateUserPassword", id)

	var data map[string]string
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	err = repository.UpdateUserPassword(currentUser, id, data["password"], data["oldPassword"], false)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}
}

/*
addUserToken
body: {"name": "for application toto", "expiration": 1586081695000}
return: {"token": "uuid"}
*/
func addUserToken(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]

	var data model.Token
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	log.Println("addUserToken", id, data.Name, data.Expiration)

	token, err := repository.AddUserToken(currentUser, id, data.Name, data.Expiration)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	json.NewEncoder(w).Encode(map[string]string{"token": token})
}

/*
addUserUnconfirmedEmail
body: {"name": "for application toto", "expiration": 1586081695000}
return: {"token": "uuid"}
*/
func addUserUnconfirmedEmail(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]

	var data model.UnconfirmedEmails
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	log.Println("addUserUnconfirmedEmail", id)

	token, err := repository.AddUserUnconfirmedEmail(currentUser, id, data.Email)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	json.NewEncoder(w).Encode(map[string]string{"token": token})

	// TODO send email confirmation with token link
}

func updateUserAuthenticationInfo(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]
	var auth model.AuthenticationInfo
	err := json.NewDecoder(r.Body).Decode(&auth)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	log.Println("updateUserAuthenticationInfo", id, auth)

	err = repository.UpdateUserAuthenticationInfo(currentUser, id, auth)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}
}

func updateUserActions(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]
	var actions []model.Action
	err := json.NewDecoder(r.Body).Decode(&actions)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	log.Println("updateUserActions", id, actions)

	err = repository.UpdateUserActions(currentUser, id, actions)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}
}

func updateUserAutoScreenshot(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]

	var data map[string]bool
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	log.Println("updateUserAutoScreenshot", id, data)

	err = repository.UpdateUserAutoScreenshot(currentUser, id, data["autoscreenshot"])
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}
}

func updateUserAutoFavicon(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]

	var data map[string]bool
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	log.Println("updateUserAutoFavicon", id, data)

	err = repository.UpdateUserAutoFavicon(currentUser, id, data["autofavicon"])
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}
}

func updateUserCollectActionHistory(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]

	var data map[string]bool
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	log.Println("updateUserCollectActionHistory", id, data)

	err = repository.UpdateCollectActionHistory(currentUser, id, data["collectactionhistory"])
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}
}

func updateUserMaxTagInCloud(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]

	var data map[string]int8
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	log.Println("updateUserMaxTagInCloud", id, data)

	err = repository.UpdateUserMaxTagInCloud(currentUser, id, data["maxtagincloud"])
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}
}

func updateUserMaxResult(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]

	var data map[string]int8
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	log.Println("updateUserMaxResult", id, data)

	err = repository.UpdateUserMaxResult(currentUser, id, data["maxresult"])
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}
}

func updateLang(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]

	var data map[string]string
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	utils.LogDebug("updateLang", id, data)

	userJSON, err := repository.UpdateLang(currentUser, id, data["lang"])
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	io.WriteString(w, userJSON)
}

func confirmUserEmail(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]
	token := mux.Vars(r)["token"]

	err := repository.ConfirmUserEmail(currentUser, id, token)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	// TODO: retourner une page indiquant que l'email est bien validé
}
