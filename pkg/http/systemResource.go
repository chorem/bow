package http

import (
	"encoding/json"
	// "io"
	"log"
	"net/http"
)

func isAlive(w http.ResponseWriter, r *http.Request) {
	log.Println("http liveness")

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]bool{"alive": true})
}

func getStats(w http.ResponseWriter, r *http.Request) {
	log.Println("http stats", stats)

	w.Header().Set("Content-Type", "application/json")
	// io.WriteString(w, stats.String())
	// je voulais faire un export json, mais ça reste vide, donc comme le toSting est un json :p
	json.NewEncoder(w).Encode(stats)
}
