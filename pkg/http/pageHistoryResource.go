package http

import (
	"io"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.chorem.org/chorem/bow/pkg/constant"
	"gitlab.chorem.org/chorem/bow/pkg/model"
	"gitlab.chorem.org/chorem/bow/pkg/repository"
	"gitlab.chorem.org/chorem/bow/pkg/utils"
)

func getPageHistory(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)
	id := mux.Vars(r)["id"]

	queryParams := r.URL.Query()
	dateAsString := queryParams.Get(constant.Date)

	date, err := time.Parse(time.RFC3339, dateAsString)
	if err != nil {
		date = time.Now()
	}

	utils.LogDebug("getPageHistory query: ", queryParams)
	json, err := repository.GetPageHistory(currentUser, id, date)
	if err != nil {
		if utils.Is404(err) {
			// on a rien retrouve, on renvoie un objet vide
			json = "{}"
		} else {
			utils.Throw(w, err)
			return
		}
	}

	w.Header().Add("Content-Type", "application/json")
	io.WriteString(w, json)
}
