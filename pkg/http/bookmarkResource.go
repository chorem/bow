package http

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.chorem.org/chorem/bow/pkg/constant"
	"gitlab.chorem.org/chorem/bow/pkg/model"
	"gitlab.chorem.org/chorem/bow/pkg/repository"
	"gitlab.chorem.org/chorem/bow/pkg/utils"
)

func getBookmarks(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)
	queryParams := r.URL.Query()
	id := queryParams.Get(constant.ID)
	uri := queryParams.Get(constant.URI)
	tags := queryParams[constant.Tags]
	fulltext := queryParams.Get(constant.Fulltext)

	utils.LogDebug("getBookmarks query: ", queryParams)
	orderBy := queryParams.Get(constant.OrderBy)
	orderAsc, err := strconv.ParseBool(queryParams.Get(constant.OrderAsc))
	if err != nil {
		orderAsc = false
	}
	first, err := strconv.ParseInt(queryParams.Get(constant.First), 10, 16)
	if err != nil {
		first = 0
	}

	json, err := repository.BookmarkJSON(currentUser, id, uri, tags, fulltext, orderBy, orderAsc, int(first))
	if err != nil {
		if utils.Is404(err) {
			// on a rien retrouve, on renvoie une collection vide
			json = "[]"
		} else {
			utils.Throw(w, err)
			return
		}
	}

	w.Header().Add("Content-Type", "application/json")
	io.WriteString(w, json)
}

func getBookmark(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)
	id := mux.Vars(r)["id"]

	json, err := repository.BookmarkByIDJSON(currentUser, id)
	if err != nil {
		utils.Throw(w, err)
		return
	}

	// on a au moins un resultat (sinon 404) on supprime les []                                                                                                                                                                
	json = json[1 : len(json)-1]                                                                                                                                                                                   

	w.Header().Add("Content-Type", "application/json")
	io.WriteString(w, json)
}

func getTags(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	query := r.URL.Query()
	filter := query.Get(constant.Filter)
	withCount, err := strconv.ParseBool(query.Get(constant.WithCount))
	if err != nil {
		withCount = false
	}

	json, err := repository.TagsJSON(currentUser, filter, withCount)
	if err != nil {
		utils.Throw(w, err)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	io.WriteString(w, json)
}

/*
renameTag utilise les valeurs dans le path 'oldName' 'newName' pour renommer
un tag.
return json {"count": <nombre de bookmark impacte>}
*/
func renameTag(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	oldName := mux.Vars(r)["oldName"]
	newName := mux.Vars(r)["newName"]

	count, err := repository.RenameTag(currentUser, oldName, newName)
	if err != nil {
		utils.Throw(w, err)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]int64{"count": count})
}

func addOneVisit(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)
	id := mux.Vars(r)["id"]

	uri, err := repository.AddOneVisit(currentUser, id)
	if err != nil {
		utils.Throw(w, err)
		return
	}

	http.Redirect(w, r, uri, http.StatusFound)
}

func addBookmark(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	var bookmark model.Bookmark
	err := json.NewDecoder(r.Body).Decode(&bookmark)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError400(err, currentUser))
		return
	}

	id, err := repository.CreateBookmark(currentUser, bookmark)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError500(err, currentUser))
		return
	}

	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"id": id})
}

func deleteBookmark(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]
	log.Println("Delete bookmark", id)
	err := repository.DeleteBookmark(currentUser, id)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}
}

/*
updateBookmark save bookmark withour AuthenticationInfo
*/
func updateBookmark(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]
	var bookmark model.Bookmark
	err := json.NewDecoder(r.Body).Decode(&bookmark)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError400(err, currentUser))
		return
	}

	bookmark.ID = id
	err = repository.UpdateBookmark(currentUser, bookmark)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError500(err, currentUser))
		return
	}
}

func updateBookmarkAuthenticationInfo(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.BowUser)

	id := mux.Vars(r)["id"]
	var auth model.AuthenticationInfo
	err := json.NewDecoder(r.Body).Decode(&auth)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	err = repository.UpdateBookmarkAuthenticationInfo(currentUser, id, auth)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}
}
