package utils

import (
	"testing"
    "regexp"
)

func TestGenUUID(t *testing.T) {
	uuid, err := GenUUID()
	if err != nil {
		t.Error("TestGenUUID", err)
	}
	matched, err := regexp.MatchString(`^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$`, uuid)
	if !matched || err != nil {
		t.Error("TestGenUUID", matched, err)
	}
}
