package utils

import (
	"log"
	"strings"
	"time"
)

/*
RemoveTagReturn remove all \n and \t in the string
*/
func RemoveTagReturn(s string) string {
	return strings.Map(func(c rune) rune {
		if c == '\n' || c == '\t' {
			return ' '
		}
		return c
	}, s)
}

// TrackTime start time tracking duration
func TrackTime(msg string) (string, time.Time) {
    return msg, time.Now()
}

// Duration compute duration from TrackTime
func Duration(msg string, start time.Time) {
    log.Printf("Timing %v: %v\n", msg, time.Since(start))
}