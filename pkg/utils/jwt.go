package utils

import (
	"github.com/brianvoe/sjwt"
	"gitlab.chorem.org/chorem/bow/pkg/model"
)

var secretKey []byte

/*
JwtInit JWT secret key
*/
func JwtInit(key []byte) {
	secretKey = key
}

/*
JwtVerify check token and if valide return user id
*/
func JwtVerify(token string) (model.BowUser, error) {
	var bowUser model.BowUser

	// check signature
	verified := sjwt.Verify(token, secretKey)
	if !verified {
		return bowUser, NewHTTPError("Can't verify token", bowUser, 401)
	}

	// read token
	claims, err := sjwt.Parse(token)
	if err != nil {
		return bowUser, NewHTTPError("Can't read token", bowUser, 401)
	}

	// check date
	err = claims.Validate()
	if err != nil {
		return bowUser, NewHTTPError("Can't validate token", bowUser, 401)
	}

	// bowUserJSON, err := claims.GetStr("user")
	// if err != nil {
	// 	return bowUser, NewHTTPError("Can't get user in token", bowUser, 401)
	// }

	// err = json.Unmarshal([]byte(bowUserJSON), &bowUser)
	// if err != nil {
	// 	return bowUser, NewHTTPError(fmt.Sprintf("Can't unmarshal user in token '%s'", bowUserJSON), bowUser, 401)
	// }

	err = claims.ToStruct(&bowUser)
	if err != nil {
		return bowUser, NewHTTPError("Can't unmarshal token", bowUser, 401)
	}

	return bowUser, nil
}

/*
JwtGenerate generate JWT token from information in parameter
*/
func JwtGenerate(user model.BowUser) (string, error) {
	// on ne garde que l'id vu que le user est rechargé a chaque requete pour etre sur de travail
	// avec des données a jour
	u := model.BowUser{ID: user.ID}
	claims, err := sjwt.ToClaims(u)
	if err != nil {
		return "", NewHTTPError500(err, user)
	}
	// claims := sjwt.New()
	// claims.Set("user", userJSON)
	jwt := claims.Generate(secretKey)

	return jwt, nil
}
