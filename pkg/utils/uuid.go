/*
Package utils contient des choses réutilisable
*/
package utils

import (
	"io/ioutil"
	"strings"
)

/*
GenUUID generate uuid.

Permet de generer des uuid sur une plateforme linux qui
est la cible de deploiement

si on vise d'autre plateforme, il faut remplacer cette
implantation par https://github.com/google/uuid
*/
func GenUUID() (string, error) {
	u, err := ioutil.ReadFile("/proc/sys/kernel/random/uuid")
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(string(u)), nil
}
