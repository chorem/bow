package utils

import (
	"fmt"
	"log"
	"net/http"
	"runtime"
	"strings"

	"gitlab.chorem.org/chorem/bow/pkg/model"
)

type codeRef struct {
	file string
	fn   string
	line int
}

/*
httpError erreur permettant de porter le code HTTP souhaite
*/
type httpError struct {
	ID            string
	Msg           string
	CurrentUserID string
	StatusCode    int
	stack         []codeRef
}

func (e *httpError) Error() string {
	return fmt.Sprintf(`{"ID": "%s", "currentUser": "%s", "StatusCode": %v, "Msg": %q}`, e.ID, e.CurrentUserID, e.StatusCode, e.Msg)
}

func addCodeRef(err *httpError) {
	pc, file, line, _ := runtime.Caller(3)
	fn := runtime.FuncForPC(pc).Name()
	pos := strings.LastIndex(fn, "/")
	pos = strings.Index(fn[pos:], ".") + pos + 1
	err.stack = append(err.stack, codeRef{file: file, fn:fn[pos:], line: line})
}

func createHTTPError(msg string, currentUser model.BowUser, statusCode int) *httpError {
	e := httpError{}
	e.ID, _ = GenUUID()
	e.Msg = msg
	e.CurrentUserID = currentUser.ID
	if statusCode > 0 {
		e.StatusCode = statusCode
	} else {
		e.StatusCode = 500
	}
	e.stack = make([]codeRef, 0, 1)
	addCodeRef(&e)
	return &e
}

func NewHTTPError(msg string, currentUser model.BowUser, statusCode int) *httpError {
	return createHTTPError(msg, currentUser, statusCode)
}

func NewHTTPError400(err error, currentUser model.BowUser) *httpError {
	if err, ok := err.(*httpError); ok {
		addCodeRef(err)
		return err
	}

	return createHTTPError(fmt.Sprintf("%v", err), currentUser, 400)
}

func NewHTTPError500(err error, currentUser model.BowUser) *httpError {
	if err, ok := err.(*httpError); ok {
		addCodeRef(err)
		return err
	}

	return createHTTPError(fmt.Sprintf("%v", err), currentUser, 500)
}

func Is404(err error) bool {
	if err, ok := err.(*httpError); ok {
		return err.StatusCode == 404
	}
	return false
}

func Throw(w http.ResponseWriter, err error) {
	if err, ok := err.(*httpError); ok {
		if err.StatusCode >= 500 || true { // FIXME faire mode debug
			log.Printf("[error] %v\n", err)
			for _, ref := range err.stack {
				log.Printf("\tin [%s#%s:%d] ", ref.file, ref.fn, ref.line)
			}
		}
		http.Error(w, fmt.Sprintf("%s", err), err.StatusCode)
	} else {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%s", err), 500)
	}
}
