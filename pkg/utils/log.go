package utils

import (
	"log"

	"gitlab.chorem.org/chorem/bow/pkg/constant"
)

/*
LogDebug log if debug level
*/
func LogDebug(msg string, a ...interface{}) {
	if constant.Debug { 
		log.Printf("[DBG] " + msg, a...)
	}
}

/*
Log log all time
*/
func Log(msg string, a ...interface{}) {
	log.Printf("[LOG] " + msg, a...)
}

/*
LogError log all time
*/
func LogError(msg string, a ...interface{}) {
	log.Printf("[ERR] " + msg, a...)
}

