package utils

import (
	"encoding/json"
	"math"
	"sync"
	"time"
)

type Stat struct {
	Nb                 int64         `json:"call"`
	Min                time.Duration `json:"min"`
	MinHumain          string
	MinInfo            string
	Max                time.Duration `json:"max"`
	MaxHumain          string
	MaxInfo            string
	Avg                time.Duration `json:"avg"`
	AvgHumain          string
	StdDeviation       time.Duration `json:"stddeviation"`
	StdDeviationHumain string

	// pour l'ecart type (standard deviation)
	delta int64
	m2    int64

	// pour que les requetes concurrente de corrompe pas les donnees
	sync.Mutex
}

type Stats struct {
	Values map[string]*Stat `json:"stats"`

	// pour que les requetes concurrente de corrompe pas les donnees
	sync.Mutex
}

type OneCall struct {
	name  string
	info  string
	start time.Time
	all   *Stats
}

func NewStats() *Stats {
	return &Stats{Values: make(map[string]*Stat)}
}

func (all *Stats) String() string {
	result, _ := json.Marshal(all)
	return string(result)
}

func (s *Stats) StartStat(name string, info string) *OneCall {
	return &OneCall{name: name, info: info, start: time.Now(), all: s}
}

func (call *OneCall) Stop() {
	duration := time.Since(call.start)
	s := call.all.Values[call.name]
	if s == nil {
		call.all.Lock()
		s = call.all.Values[call.name]
		if s == nil {
			s = &Stat{}
			call.all.Values[call.name] = s
		}
		call.all.Unlock()
	}

	s.Lock()
	defer s.Unlock()

	s.Nb++
	if s.Min == 0 || s.Min > duration {
		s.Min = duration
		s.MinHumain = duration.String()
		s.MinInfo = call.info
	}

	if s.Max < duration {
		s.Max = duration
		s.MaxHumain = duration.String()
		s.MaxInfo = call.info
	}

	s.delta = duration.Nanoseconds() - s.Avg.Nanoseconds()
	s.Avg = time.Duration(s.Avg.Nanoseconds() + s.delta/s.Nb)
	s.AvgHumain = s.Avg.String()
	s.m2 = s.m2 + s.delta*(duration.Nanoseconds()-s.Avg.Nanoseconds())

	variance := float64(s.m2) / math.Max(1, float64(s.Nb-1))
	s.StdDeviation = time.Duration(int64(math.Sqrt(variance)))
	s.StdDeviationHumain = s.StdDeviation.String()
}
