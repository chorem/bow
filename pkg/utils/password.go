/*
Package utils contient des choses réutilisable
*/
package utils

import (
	"crypto/md5"
	"fmt"

	"golang.org/x/crypto/bcrypt"
)

/*
HashPassword return hash for the given password
currently bcrypt is used
*/
func HashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte([]byte(password)), 8)
	return string(hashedPassword), err
}

/*
CheckPassword compare password and hash, return true if password proceduce this hash.
Compare is done on all version of password encoding in this order:
- bcrypt
- md5
*/
func CheckPassword(password string, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		// try with old md5
		md5hash := md5.Sum([]byte(password))
		return fmt.Sprintf("%x", md5hash) == hash
	}

	return true
}
