package utils

import (
	"bytes"
	"encoding/base64"
	"image"
	// on veut le support des gif
	_ "image/gif"
	// on veut le support des jpeg
	_ "image/jpeg"
	"image/png"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"

	"gitlab.chorem.org/chorem/bow/pkg/constant"
	"golang.org/x/image/draw"
)

// GetScreenShot recupere le screenshot de la page
func GetScreenShot(uri string) (string, string) {
	if constant.ScreenShotURL == "" {
		return "", ""
	}

	ssu := strings.ReplaceAll(constant.ScreenShotURL, "{url}", url.QueryEscape(uri))
	log.Println("get screenshot from ", ssu)
	resp, err := http.Get(ssu)
	if err != nil {
		log.Printf("[error] %v\n", err)
		return "", ""
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("[error] %v\n", err)
		return "", ""
	}

	buffer := bytes.NewBuffer(body)
	loadedImage, _, err := image.Decode(buffer)

	mini := resize(loadedImage, 65, 91)
	maxi := base64.StdEncoding.EncodeToString(body)

	return mini, maxi
}

func resize(src image.Image, x int, y int) string {
	rect := image.Rect(0, 0, x, y)
	// resize using given scaler
	dst := image.NewRGBA(rect)
	draw.NearestNeighbor.Scale(dst, rect, src, src.Bounds(), draw.Over, nil)

	var buff bytes.Buffer
	err := png.Encode(&buff, dst)

	if err != nil {
		return ""
	}

	encodedString := base64.StdEncoding.EncodeToString(buff.Bytes())
	return encodedString

}

// GetFavicon recupere le favicon de la page
func GetFavicon(uri string) string {
	if constant.FaviconURL == "" {
		return ""
	}

	ssu := strings.ReplaceAll(constant.FaviconURL, "{url}", url.QueryEscape(uri))
	log.Println("get favicon from ", ssu)
	resp, err := http.Get(ssu)
	if err != nil {
		log.Printf("[error] %v\n", err)
		return ""
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return ""
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("[error] %v\n", err)
		return ""
	}

	buffer := bytes.NewBuffer(body)
	loadedImage, _, err := image.Decode(buffer)

	fav := resize(loadedImage, 24, 24)

	return fav
}
