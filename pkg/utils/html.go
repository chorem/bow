package utils

import (
	"encoding/json"
	"log"
	"net/http"
	"net/url"
	"strings"

	"gitlab.chorem.org/chorem/bow/pkg/constant"
)

// GetHTML recupere le screenshot de la page
func GetHTML(uri string) string {
	if constant.HTMLExtractorURL == "" {
		return ""
	}

	ssu := strings.ReplaceAll(constant.HTMLExtractorURL, "{url}", url.QueryEscape(uri))
	log.Println("get html from ", ssu)
	resp, err := http.Get(ssu)
	if err != nil {
		log.Printf("[error] %v\n", err)
		return ""
	}

	defer resp.Body.Close()
	// body, err := ioutil.ReadAll(resp.Body)
	// if err != nil {
	// 	log.Printf("[error] %v\n", err)
	// 	return ""
	// }

	var html string
	if err := json.NewDecoder(resp.Body).Decode(&html); err != nil {
		log.Printf("[error] %v\n", err)
		return ""
	}

	return html
}
