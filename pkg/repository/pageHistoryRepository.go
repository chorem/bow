package repository

import (
	"time"

	"encoding/json"

	"gitlab.chorem.org/chorem/bow/pkg/constant"
	"gitlab.chorem.org/chorem/bow/pkg/model"
	"gitlab.chorem.org/chorem/bow/pkg/utils"
)

/*
CreatePageHistory creation d'un nouvel historique d'action
return: id, err
*/
func CreatePageHistory(uri string, content string) string {

	id, err := utils.GenUUID()
	if err != nil {
		utils.LogError("can't insert page history %v\n", err)
		return ""
	}

	h := model.PageHistory{ID: id, CreationDate: time.Now(), URI: uri, Content: content, Lang: "english"}

	hAsJSON, err := json.Marshal(h)
	if err != nil {
		utils.LogError("can't insert page history %v\n", err)
		return ""
	}

	utils.LogDebug("Creation de l'history pour l'url '%s'\n", uri)
	q := &query{sql: `INSERT INTO pageHistory AS t SELECT * FROM json_populate_record(NULL::pageHistory, $1::json);`}
	err = q.execOnOneRow(constant.Nobody, string(hAsJSON))
	if err != nil {
		utils.LogError("can't insert page history %v\n", err)
		return ""
	}

	return id
}

// GetPageHistory retourne une page d'historique spécifique (date), on retourne la date plus petit si date n'est pas trouvée
func GetPageHistory(currentUser model.BowUser, id string, date time.Time) (string, error) {
	var result string
	var err error

	maxResult := currentUser.MaxResult
	if maxResult == 0 || maxResult > 1000 {
		maxResult = 100
	}

	utils.LogDebug("search page history uri: %v, date: '%v'", id, date)

	q := &query{sql: `WITH
	__url AS (select uri from bookmark where id=$1),
	__info AS (select min(creationdate) as first, max(creationdate) as last, count(creationdate) as count from pageHistory p, __url where p.uri = __url.uri),
	__data AS (select p.uri, content, creationdate, LEAD(creationdate) OVER (ORDER BY creationdate) AS next, LAG(creationdate) OVER (ORDER BY creationdate) AS prev from pageHistory p, __url where p.uri=__url.uri order by creationdate <-> $2 limit 1)
	select row_to_json(v) from (select * from __data, __info) v
	`}
	result, err = q.QueryString(currentUser, id, date)

	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	return result, nil

}
