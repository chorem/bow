package repository

import (
	"time"

	"encoding/json"

	"gitlab.chorem.org/chorem/bow/pkg/model"
	"gitlab.chorem.org/chorem/bow/pkg/utils"
)

/*
CreateActionHistory creation d'un nouvel historique d'action
return: id, err
*/
func CreateActionHistory(currentUser model.BowUser, action model.Action, request string) (string, error) {

	id, err := utils.GenUUID()
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	h := model.ActionHistory{ID: id, Owner: currentUser.ID, CreationDate: time.Now(), Action: action, Request: request , Lang: "english"}

	hAsJSON, err := json.Marshal(h)
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	utils.LogDebug("Creation de l'history %s\n", hAsJSON)
	q := &query{sql: `INSERT INTO actionHistory AS t SELECT * FROM json_populate_record(NULL::actionHistory, $1::json);`}
	err = q.execOnOneRow(currentUser, string(hAsJSON))
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	return id, err
}
