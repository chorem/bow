package repository

import (
	"gitlab.chorem.org/chorem/bow/pkg/constant"
	"gitlab.chorem.org/chorem/bow/pkg/utils"
)

/*
CreateOrUpdatePageInfo creation ou mise a jour des infos
return: id, err
*/
func CreateOrUpdatePageInfo(uri string, favicon string, mini string, maxi string) {
	utils.LogDebug("Update info for %s\n", uri)

	q := &query{sql: `
	INSERT INTO pageInfo AS i (uri, favicon, miniscreenshot, screenshot) values ($1, $2, $3, $4)
	ON CONFLICT(uri) DO UPDATE SET favicon = COALESCE(EXCLUDED.favicon, i.favicon), miniscreenshot = COALESCE(EXCLUDED.miniscreenshot, i.miniscreenshot), screenshot = COALESCE(EXCLUDED.screenshot, i.screenshot);`}
	err := q.execOnOneRow(constant.Nobody, uri, emptyToNil(favicon), emptyToNil(mini), emptyToNil(maxi))

	if err != nil {
		utils.LogError("can't insert or update pageInfo %v\n", err)
	}
}

func emptyToNil(s string) interface{} {
	if s == "" {
		return nil
	}
	return s
}
