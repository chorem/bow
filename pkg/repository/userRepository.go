package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/jackc/pgtype"
	"gitlab.chorem.org/chorem/bow/pkg/constant"
	"gitlab.chorem.org/chorem/bow/pkg/model"
	"gitlab.chorem.org/chorem/bow/pkg/utils"
)

/*
UserJSON return user in json format
all field are send except:
- password
- email confirmation token
*/
func UserJSON(currentUser model.BowUser, id string, fields ...string) (string, error) {
	var askedFields string
	if len(fields) == 0 {
		askedFields = "id, creationdate, updatedate, admin, login, tokens, emails, unconfirmedemails, authenticationinfo, autoscreenshot, autofavicon, collectactionhistory, maxtagincloud, maxresult, actions, lang"
	} else {
		askedFields = strings.Join(fields, ", ")
	}

	allFields := strings.ReplaceAll(askedFields, "unconfirmedemails,", "")

	q := &query{sql: fmt.Sprintf(`
		WITH unconfirmedemailsList as (select id as eid, jsonb_array_elements(unconfirmedemails)::jsonb->'email' as unconfirmedemailsList from bowuser u where id=$1),
		     tmp AS (select %[1]s, array_agg(unconfirmedemailsList) as unconfirmedemails from bowuser left join unconfirmedemailsList on eid=id where id=$1 GROUP BY %[1]s),
		     __all AS (select %[2]s from tmp)
		SELECT json_agg(__all.*) as j
		FROM __all`, allFields, askedFields)}
	result, err := q.QueryString(currentUser, id)
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	result = result[1 : len(result)-1] // suppression des []

	return result, nil
}

/*
User return user
all field are send except:
- password
- email confirmation token
*/
func User(currentUser model.BowUser, id string, fields ...string) (model.BowUser, error) {
	var user model.BowUser
	result, err := UserJSON(currentUser, id, fields...)
	if err != nil {
		return user, utils.NewHTTPError500(err, currentUser)
	}

	err = json.Unmarshal([]byte(result), &user)
	if err != nil {
		return user, utils.NewHTTPError500(err, currentUser)
	}

	return user, nil
}

/*
UserFromToken get user id  by application token
*/
func UserFromToken(token string, fields ...string) (model.BowUser, error) {
	currentUser := constant.Nobody

	var user model.BowUser

	var allFields string
	if len(fields) == 0 {
		allFields = "id, creationdate, updatedate, admin, login, tokens, emails, authenticationinfo, autoscreenshot, autofavicon, maxtagincloud, maxresult, actions"
	} else {
		allFields = strings.Join(fields, ",")
	}

	tokenJSON := fmt.Sprintf(`{"token": "%s"}`, token)
	q := &query{sql: fmt.Sprintf(`
		WITH __all AS (select %s from bowuser b
		  where exists (select * from jsonb_array_elements(tokens) as x
			where x @> $1))
		SELECT json_agg(__all) as j
			FROM __all;`, allFields)}
	result, err := q.QueryString(currentUser, tokenJSON)
	if err != nil {
		return user, utils.NewHTTPError500(err, currentUser)
	}

	result = result[1 : len(result)-1] // suppression des []
	err = json.Unmarshal([]byte(result), &user)
	if err != nil {
		return user, utils.NewHTTPError500(err, currentUser)
	}

	return user, nil
}

/*
CheckUserPasswordForID check password for id
*/
func CheckUserPasswordForID(id string, password string) bool {
	var hash string
	row := db.QueryRow(context.Background(), `select password from bowuser where id=$1`, id)
	err := row.Scan(&hash)
	if err != nil {
		return false
	}

	return utils.CheckPassword(password, hash)
}

/*
CheckUserPasswordForEmail retourne l'id de l'utilisateur (string) si le mot de passe est le bon et que l'utilisateur est retrouve
*/
func CheckUserPasswordForEmail(loginOrEmail string, password string) (string, error) {
	var uuid pgtype.UUID
	var hash string
	row := db.QueryRow(context.Background(), `select id, password from bowuser where login=$1 or emails @> $2::text[]`, loginOrEmail, fmt.Sprintf(`{"%s"}`, loginOrEmail))
	err := row.Scan(&uuid, &hash)
	if err != nil {
		return "", err
	}

	if !utils.CheckPassword(password, hash) {
		return "", utils.NewHTTPError(fmt.Sprintf("Password mismatch for %s", loginOrEmail), constant.Nobody, 401)
	}

	var result string
	err = uuid.AssignTo(&result)
	if err != nil {
		return "", err
	}

	return result, nil
}

/*
CreateUser retourne l'utilisateur au format json
*/
func CreateUser(login string, password string) (string, error) {
	hashPassword, err := utils.HashPassword(password)
	if err != nil {
		return "", utils.NewHTTPError500(err, model.BowUser{ID: login})
	}

	uuid, err := utils.GenUUID()
	if err != nil {
		return "", utils.NewHTTPError500(err, model.BowUser{ID: login})
	}

	currentUser := model.BowUser{
		ID: uuid, Login: login, Password: hashPassword, MaxTagInCloud: 20, MaxResult: 20, AutoFavicon: false, AutoScreenshot: false, AuthenticationInfo: &model.AuthenticationInfo{DomainComponent: 2, MaxLength: 15}}
	userAsJSON, err := json.Marshal(currentUser)
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	log.Println("create user", string(userAsJSON))

	q := &query{asNobody: true, sql: `INSERT INTO bowUser AS t SELECT * FROM json_populate_record(NULL::bowUser, $1::json);`}
	err = q.execOnOneRow(currentUser, userAsJSON)

	return currentUser.ID, err
}

/*
DeleteUser suppression d'un nouveau bookmark
*/
func DeleteUser(currentUser model.BowUser, id string) error {
	q := &query{sql: `DELETE FROM bowuser WHERE id=$1;`}
	err := q.execOnOneRow(currentUser, id)

	return err
}

/*
UpdateUserPassword update user password, if old password match, or if force is true
*/
func UpdateUserPassword(currentUser model.BowUser, id string, password string, oldPassword string, force bool) error {
	if !force && !CheckUserPasswordForID(id, oldPassword) {
		return utils.NewHTTPError(fmt.Sprintf("Bad old password for user '%s'", id), currentUser, 400)
	}

	hash, err := utils.HashPassword(password)
	if err != nil {
		return utils.NewHTTPError500(err, currentUser)
	}

	q := &query{sql: `update bowuser SET password=$2 where id=$1`}
	err = q.execOnOneRow(currentUser, id, hash)

	return err
}

/*
AddUserToken ajout un tocken d'authentification pour l'utilisateur
*/
func AddUserToken(currentUser model.BowUser, id string, name string, expiration *time.Time) (string, error) {
	token, err := utils.GenUUID()
	if err != nil {
		return "", err
	}

	json, err := json.Marshal(model.Token{Name: name, Token: token, Expiration: expiration})
	if err != nil {
		return token, err
	}

	q := &query{sql: `update bowuser SET tokens=coalesce(tokens, '[]'::jsonb) || $2::jsonb where id=$1;`}
	err = q.execOnOneRow(currentUser, id, json)

	return token, err
}

/*
AddUserUnconfirmedEmail ajout d'un email non confirme, retourne le token permettant la confirmation
*/
func AddUserUnconfirmedEmail(currentUser model.BowUser, id string, email string) (string, error) {
	token, err := utils.GenUUID()
	if err != nil {
		return "", err
	}

	json, err := json.Marshal(model.UnconfirmedEmails{Email: email, Token: token, CreationDate: time.Now()})
	if err != nil {
		return token, err
	}

	q := &query{sql: `update bowuser SET unconfirmedemails=coalesce(unconfirmedemails, '[]'::jsonb) || $2::jsonb where id=$1;`}
	err = q.execOnOneRow(currentUser, id, json)

	return token, nil
}

/*
UpdateUserAuthenticationInfo met a jour les infos d'authentification
*/
func UpdateUserAuthenticationInfo(currentUser model.BowUser, id string, auth model.AuthenticationInfo) error {
	authAsJSON, err := json.Marshal(auth)
	if err != nil {
		return err
	}

	q := &query{sql: `UPDATE bowuser
		SET (authenticationinfo) = (SELECT * FROM json_populate_record(NULL::authenticationinfo, $2::json))
		WHERE id=$1`}
	err = q.execOnOneRow(currentUser, id, string(authAsJSON))

	return err
}

/*
UpdateUserActions met a jour les actions utilisateur
*/
func UpdateUserActions(currentUser model.BowUser, id string, actions []model.Action) error {
	json, err := json.Marshal(actions)
	if err != nil {
		return err
	}

	q := &query{sql: `update bowuser SET actions=$2::jsonb where id=$1;`}
	err = q.execOnOneRow(currentUser, id, json)

	return err
}

/*
UpdateUserAutoScreenshot met a jour le boolean d'auto screenshot de la page
*/
func UpdateUserAutoScreenshot(currentUser model.BowUser, id string, value bool) error {
	q := &query{sql: `update bowuser SET autoScreenshot=$2 where id=$1;`}
	err := q.execOnOneRow(currentUser, id, value)

	return err
}

/*
UpdateUserAutoFavicon met a jour le boolean d'auto favicon de la page
*/
func UpdateUserAutoFavicon(currentUser model.BowUser, id string, value bool) error {
	q := &query{sql: `update bowuser SET autoFavicon=$2 where id=$1;`}
	err := q.execOnOneRow(currentUser, id, value)

	return err
}

/*
UpdateCollectActionHistory met a jour le boolean d'auto favicon de la page
*/
func UpdateCollectActionHistory(currentUser model.BowUser, id string, value bool) error {
	q := &query{sql: `update bowuser SET collectActionHistory=$2 where id=$1;`}
	err := q.execOnOneRow(currentUser, id, value)

	return err
}

/*
UpdateUserMaxTagInCloud met a jour le nombre d'element du nuage de tag
*/
func UpdateUserMaxTagInCloud(currentUser model.BowUser, id string, value int8) error {
	q := &query{sql: `update bowuser SET maxTagInCloud=$2 where id=$1;`}
	err := q.execOnOneRow(currentUser, id, value)

	return err
}

/*
UpdateUserMaxResult met a jour le nombre d'element affiché dans une page de resultat
*/
func UpdateUserMaxResult(currentUser model.BowUser, id string, value int8) error {
	q := &query{sql: `update bowuser SET maxResult=$2 where id=$1;`}
	err := q.execOnOneRow(currentUser, id, value)

	return err
}

/*
UpdateLang met a jour la lang de l'utilisateur
*/
func UpdateLang(currentUser model.BowUser, id string, value string) (string, error) {
	fields := strings.Join(constant.AuthFields, ",")
	q := &query{sql: fmt.Sprintf(`WITH __all AS (UPDATE bowuser SET lang=$2 WHERE id=$1 RETURNING %s) SELECT json_agg(__all)->0 AS json FROM __all;`, fields)}
	userJSON, err := q.QueryString(currentUser, id, value)

	return userJSON, err
}

/*
ConfirmUserEmail verif et confirme un email
*/
func ConfirmUserEmail(currentUser model.BowUser, id string, token string) error {
	// le but est de simplement faire passer du statut non confirmer à confirmer un email
	// si on le retrouve pour l'utilisateur et que le token est le bon.
	// mais avec le schema de base choisi c'est un poil compliqué
	// - on recheche les emails non confirmer du user et on les transformes le json en ligne
	// - on ajoute la position de cette email (numero de ligne)
	// - on ne garde que la ligne de l'email concerné
	// - on update la ligne en faisant passer l'email dans les emails valides (ajout dans emails, suppression dans unconfirmedemails)
	// - on garanti que personne n'a deja cette email, sinon on ne fait rien (il reste en a confirmer)
	jsonPathToCheckToken := fmt.Sprintf(`$[*].token ? (@ == "%s")`, token)

	q := &query{sql: `
	with ue as (select jsonb_array_elements(unconfirmedemails) as json
				from bowuser
				where b.id=$1 and unconfirmedemails @? $3),
		 re as (select ROW_NUMBER () OVER () as index, *
				from ue),
		 data as (select index, json->>'email' as email
				  from re
				  where json->>'token' = $2)
	update bowuser as b
		   set
			   emails=array_append(b.emails, data.email),
			   unconfirmedemails=unconfirmedemails - (data.index::int - 1)
		   from data
		   where b.id=$1 and b.unconfirmedemails @? $3 and
		         (select count(id) from bowuser where emails @> ('{' || data.email || '}')::text[]) = 0;`}
	err := q.execOnOneRow(currentUser, id, token, jsonPathToCheckToken)

	return err
}
