package repository

import (
	"fmt"
	"strings"
	"time"

	"encoding/json"

	"gitlab.chorem.org/chorem/bow/pkg/model"
	"gitlab.chorem.org/chorem/bow/pkg/utils"
)

func collectPageInfo(uri string) {
	favicon := utils.GetFavicon(uri)
	mini, maxi := utils.GetScreenShot(uri)

	CreateOrUpdatePageInfo(uri, favicon, mini, maxi)
}

func createPageHistory(uri string) {
	content := utils.GetHTML(uri)
	CreatePageHistory(uri, content)
}

/*
BookmarkJSON retourne le bookmark au format json
si id est non vide alors fait une recherche exact sur l'id
si uri est non vide alors fait une recherche exact sur l'uri
sinon fait une recherche sur les critères: tags, fulltext
*/
func BookmarkJSON(currentUser model.BowUser, id string, uri string, tags []string, fulltext string, orderBy string, orderAsc bool, first int) (string, error) {
	var result string
	var err error

	maxResult := currentUser.MaxResult
	if maxResult == 0 || maxResult > 1000 {
		maxResult = 100
	}

	// normalize orderBy to prevent SQL injection
	orderBy = bookmarkFields[orderBy]
	if orderBy == "" {
		orderBy = "creationdate"
	}

	orderDirection := ""
	if !orderAsc {
		orderDirection = "desc"
	}

	tagsJoined := strings.Join(tags, ",")

	utils.LogDebug("search bookmark id: %v, uri: %v, tags: '%v', fulltext: '%v',  orderBy: %v, orderAsc: %v, first:%v", id, uri, tags, fulltext, orderBy, orderAsc, first)
	var mainQuery string
	if id != "" {
		mainQuery = `select b.*, i.favicon, i.miniscreenshot, i.screenshot from bookmark b left join pageInfo i on b.uri=i.uri where b.id=$4`
	} else if uri != "" {
		mainQuery = `select b.*, i.favicon, i.miniscreenshot, i.screenshot from bookmark b left join pageInfo i on b.uri=i.uri where b.uri=$3`
	} else {
		// si fulltext and vide, aucun resultat n'est retourne, ce n'est pas ce qu'on veut
		// dans ce cas, on souhaite que la recherche ne porte que sur les tags
		queryEmptyFulltext := ""
		if fulltext == "" {
			queryEmptyFulltext = "true OR "
		}

		// highlighted description => hldescription
		hl := ""
		// il faut que fulltext ou tags soit non vide pour mettre en highlighted les tags
		hitags := make([]string, 0, 2)
		if fulltext != "" {
			hl = `, ts_headline(description, websearch_to_tsquery($2), 'HighlightAll=true') AS hldescription`
			hitags = append(hitags, `websearch_to_tsquery($2)`)
		}

		if tagsJoined != "" {
			hitags = append(hitags, `plainto_tsquery($1)`)
		}

		hitagsJoined := strings.Join(hitags, " || ")
		if hitagsJoined != "" {
			hl = fmt.Sprintf(`%s, string_to_array(ts_headline(array_to_string(tags, ','), %s, 'HighlightAll=true'), ',') AS hltags`, hl, hitagsJoined)
		}

		// vu qu'on veut utiliser l'index fulltext créer, il faut les meme champs dans la requete (to_tsvector)
		mainQuery = fmt.Sprintf(`select
			b.*, i.favicon, i.miniscreenshot, i.screenshot%s
			FROM bookmark b left join pageInfo i on b.uri=i.uri WHERE
			tags @> string_to_array($1, ',')
			AND
			(%s to_tsvector(lang, text(coalesce(tags, '{}'::text[])) || ' ' || coalesce(description, '') || ' ' || coalesce(b.uri, '') || ' ' || text(coalesce(privateAlias, '{}'::text[])) || ' ' || text(coalesce(publicAlias, '{}'::text[]))) @@ websearch_to_tsquery($2))
			`, hl, queryEmptyFulltext)
	}

	q := &query{sql: fmt.Sprintf(`WITH
		 __query AS (%[5]s),
		 __info AS (select %[3]d as first, %[4]d as limit, count(id) as total, '%[1]s' as orderby, to_json('%[2]s' <> 'desc') as orderasc, string_to_array($1, ',') as tags, $2::TEXT as fulltext, $3::TEXT as uri, $4::TEXT as id from __query),
		 __infoJson AS (select row_to_json(i.*) as info from __info i),
		 __allTags AS (select unnest(tags) as tag from __query),
		 __tags AS (select tag, count(tag) from __allTags group by tag order by 2 desc),
		 __jsonTags AS (select ('[' || json_agg(a.tag) || ', ' || json_agg(a.count) || ']')::json as tags from __tags a),
		 __result AS (select * from __query order by %[1]s %[2]s OFFSET %[3]d LIMIT %[4]d),
		 __jsonResult AS (SELECT json_agg(r.*) as result FROM __result r)
		 select row_to_json(v) from (select * from __infoJson, __jsonTags, __jsonResult) v;
	`, orderBy, orderDirection, first, maxResult, mainQuery)}
	result, err = q.QueryString(currentUser, tagsJoined, fulltext, uri, id)

	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	return result, nil
}

/*
BookmarkByIDJSON retourne le bookmark au format json
*/
func BookmarkByIDJSON(currentUser model.BowUser, id string) (string, error) {
	q := &query{sql: `WITH
		 __query AS (select b.*, i.favicon, i.miniscreenshot, i.screenshot from bookmark b left join pageInfo i on b.uri=i.uri where b.id=$1)
		 select json_agg(q.*) as result from __query q;
		 `}
	result, err := q.QueryString(currentUser, id)

	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	return result, nil
}

/*
RenameTag renomme ou supprime un tag si newName est vide
*/
func RenameTag(currentUser model.BowUser, oldName string, newName string) (int64, error) {
	// tagArray := "{" + oldName + "}"

	var q *query
	var count int64
	var err error
	if newName == "" {
		q = &query{sql: `UPDATE bookmark SET tags=array_remove(tags, $2) where tags @> ARRAY[$1]`}
		count, err = q.execOnNRow(currentUser, oldName, oldName)
	} else {
		q = &query{sql: `UPDATE bookmark SET tags=array_replace(tags, $2, $3) where tags @> ARRAY[$1]`}
		count, err = q.execOnNRow(currentUser, oldName, oldName, newName)
	}
	if err != nil {
		return 0, utils.NewHTTPError500(err, currentUser)
	}

	return count, nil
}

/*
TagsJSON retourne la liste des tags qui match le filtre
le format de retour est celui d'opensearch
si on demande avec le count, on a un tableau de tag, suivi du tableau du nombre d'occurence de ce tag:
[["tag1", "tag2", ...], [12, 34, ...]]
*/
func TagsJSON(currentUser model.BowUser, filter string, withCount bool) (string, error) {
	var q *query
	if withCount {
		q = &query{sql: `WITH __all AS (select unnest(tags) as tag from bookmark), __some AS (select tag, count(tag) from __all where tag ilike $1 group by tag order by 2 desc) select '[' || json_agg(a.tag) || ', ' || json_agg(a.count) || ']' as suggestion from __some a;`}
	} else {
		q = &query{sql: `WITH __all AS (select distinct unnest(tags) as tag from bookmark order by 1), __some AS (select * from __all where tag ilike $1) select json_agg(tag) as suggestion  from __some;`}
	}
	substring := fmt.Sprintf("%%%s%%", filter) // to do like, with must have many % :)
	result, err := q.QueryString(currentUser, substring)
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	return result, nil
}

/*
URIsFromAliasJSON retourne les uris associees au bout d'alias prive une recherche ilike %% est fait
c'est utile pour les suggestions
*/
func URIsFromAliasJSON(currentUser model.BowUser, alias string) (string, error) {
	arg := fmt.Sprintf(`%%%s%%`, alias)
	// recherche que dans les alias prive
	q := &query{sql: `select json_agg(uri) from bookmark where text(privateAlias) ilike $1;`}
	result, err := q.QueryString(currentUser, arg)
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	return result, nil
}

/*
URIFromAlias retourne l'uri associe a l'alias prive ou public
*/
func URIFromAlias(currentUser model.BowUser, alias string) (string, error) {
	arg := fmt.Sprintf(`{"%s"}`, alias)
	// recherche dans les alias prive en 1er
	q := &query{sql: `select uri from bookmark where privateAlias && $1::text[];`}
	result, err := q.QueryString(currentUser, arg)
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	if result == "" {
		// s'il n'y a pas de resultat recherche dans les alias public
		q = &query{asNobody: true, sql: `select uri from bookmark where publicAlias && $1::text[];`}
		result, err = q.QueryString(currentUser, arg)
		if err != nil {
			return "", utils.NewHTTPError500(err, currentUser)
		}
	}

	return result, nil
}

// AddOneVisit ajout 1 au compteur de visite
func AddOneVisit(currentUser model.BowUser, id string) (string, error) {
	q := &query{sql: `update bookmark SET visit = coalesce(visit, 0) + 1 where id=$1 returning uri;`}
	uri, err := q.QueryString(currentUser, id)
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	return uri, nil
}

/*
CreateBookmark creation d'un nouveau bookmark
return: id, err
*/
func CreateBookmark(currentUser model.BowUser, bookmark model.Bookmark) (string, error) {

	id, err := utils.GenUUID()
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	bookmark.ID = id
	bookmark.Owner = currentUser.ID
	bookmark.CreationDate = time.Now()
	bookmark.UpdateDate = bookmark.CreationDate
	bookmark.Visit = 0
	bookmark.Lang = "english"

	bookmarkAsJSON, err := json.Marshal(bookmark)
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	utils.LogDebug("Creation du bookmark %s\n", bookmarkAsJSON)
	q := &query{sql: `INSERT INTO bookmark AS t SELECT * FROM json_populate_record(NULL::bookmark, $1::json);`}
	err = q.execOnOneRow(currentUser, string(bookmarkAsJSON))
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	go collectPageInfo(bookmark.URI)
	go createPageHistory(bookmark.URI)

	return id, err
}

/*
UpdateBookmark creation d'un nouveau bookmark
*/
func UpdateBookmark(currentUser model.BowUser, bookmark model.Bookmark) error {
	bookmarkAsJSON, err := json.Marshal(bookmark)
	if err != nil {
		return utils.NewHTTPError500(err, currentUser)
	}

	utils.LogDebug("Update bookmark", bookmarkAsJSON)
	q := &query{sql: `
	    UPDATE bookmark AS t
    	SET (uri, description, tags, privateAlias, publicAlias, lang) =
            (SELECT uri, description, tags, privateAlias, publicAlias, lang
			 FROM json_populate_record(NULL::bookmark, $2::json))
	    WHERE id=$1`}
	err = q.execOnOneRow(currentUser, bookmark.ID, string(bookmarkAsJSON))
	if err != nil {
		return utils.NewHTTPError500(err, currentUser)
	}

	go collectPageInfo(bookmark.URI)
	go createPageHistory(bookmark.URI)

	return err
}

/*
UpdateBookmarkAuthenticationInfo creation d'un nouveau bookmark
*/
func UpdateBookmarkAuthenticationInfo(currentUser model.BowUser, id string, auth model.AuthenticationInfo) error {
	authAsJSON, err := json.Marshal(auth)
	if err != nil {
		return err
	}

	q := &query{sql: `
	    UPDATE bookmark AS t
		SET (authenticationinfo) = (SELECT * FROM json_populate_record(NULL::authenticationinfo, $2::json))
		WHERE id=$1`}
	err = q.execOnOneRow(currentUser, id, string(authAsJSON))

	return err
}

/*
DeleteBookmark suppression d'un nouveau bookmark
*/
func DeleteBookmark(currentUser model.BowUser, id string) error {
	q := &query{sql: `DELETE FROM bookmark WHERE id=$1 RETURNING uri`}
	err := q.execOnOneRow(currentUser, id)

	return err
}
