package constant

import (
	"os"

	"gitlab.chorem.org/chorem/bow/pkg/model"
)

// Debug true si on est en debug
var Debug = os.Getenv("BOW_DEBUG") == "true"

// ScreenShotURL l'url du service de screenshot
var ScreenShotURL = os.Getenv("BOW_SCREENSHOT_URL")

// FaviconURL l'url du service de favicon
var FaviconURL = os.Getenv("BOW_FAVICON_URL")

// HTMLExtractorURL l'url du service qui extrait le content html de la page
var HTMLExtractorURL = os.Getenv("BOW_HTML_EXTRACTOR_URL")

type httpRequestContexttKey string

/*
Nobody l'utilisateur à utiilser s'il n'y a personne d'authentifie
*/
var Nobody = model.BowUser{ID: "nobody"}

/*
User le nom utiliser pour les infos user dans les cookies
*/
const User = "bow-user"

/*
Token le nom utiliser pour le token dans les cookies et les parameters de query
*/
const Token = "bow-token"

/*
AuthFields la liste des champs necessaire pour l'utilisation du user sur le front et le back'
*/
var AuthFields = []string{"id", "admin", "login", "creationdate", "maxtagincloud", "maxresult", "actions", "lang"}

/*
TokenHeader le nom utiliser pour mettre dans le header de requete http (fallback Authorization)
*/
const TokenHeader = "x-" + Token

/*
Filter constante pour le query param pour recupere des tags
*/
const Filter = "filter"

/*
WithCount indique de retourner le nombre d'occurrence de ce tag
*/
const WithCount = "with-count"

/*
Action query parameter name for tags search
*/
const Action = "action"

/*
Suggestion query parameter name for tags search
*/
const Suggestion = "suggestion"

/*
Tags query parameter name for tags search
*/
const Tags = "tags"

/*
Fulltext query parameter name for fulltext search
*/
const Fulltext = "fulltext"

/*
Query query parameter name for fulltext search
*/
const Query = "query"

/*
ID query parameter name for id search
*/
const ID = "id"

/*
URI query parameter name for uri search
*/
const URI = "uri"

/*
OrderBy query parameter name for select order sorting
*/
const OrderBy = "orderby"

/*
OrderAsc query parameter name for select order sorting direction descending
*/
const OrderAsc = "orderasc"

/*
First query parameter name first result
*/
const First = "first"

/*
Date use to specify pageHistory date
*/
const Date = "date"

/*
Save string action to save bookmark
*/
const Save = "search.add"

/*
RedirectAlias string action to redirect to alias (private or public)
*/
const RedirectAlias = "search.alias"

/*
SuggestionAlias string action to suggest alias (private)
*/
const SuggestionAlias = "suggestion.alias"

/*
SearchFulltext string action to fulltext search in all bookmarks
*/
const SearchFulltext = "search.fulltext"

/*
SuggestionFulltext string action to suggest url from fulltext search in all alias
*/
const SuggestionFulltext = "suggestion.fulltext"

/*
SearchTag string action to search on tag in all bookmarks
*/
const SearchTag = "search.tag"

/*
SuggestionTag string action to suggest url from tag in all alias
*/
const SuggestionTag = "suggestion.tag"
