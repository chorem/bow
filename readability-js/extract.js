function extract() {
  let article = new Readability(document).parse()
  let result =  DOMPurify.sanitize( article.content )
  return result
}
