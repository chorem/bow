# Mise à jour de la police d'icônes

* Ajout d'icônes au format svg dans le dossier ```docs/assets/icons/svg```
* Exécuter le script ```fontcustom.sh```

# Source des icônes

* https://tablericons.com/
