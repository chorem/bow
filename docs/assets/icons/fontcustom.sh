#!/bin/bash

sudo rm -rf output

docker run --rm --name fc -v $(pwd):/work -ti -d thomaswelton/fontcustom
docker exec -it fc fontcustom compile /work/svg -c /work/fontcustom.yml -o /work/output
docker stop fc

# Installe la font générée
cp output/bow-icons.ttf ../../../web/src/assets/font/
cp output/bow-icons.svg ../../../web/src/assets/font/
cp output/bow-icons.woff ../../../web/src/assets/font/
cp output/bow-icons.eot ../../../web/src/assets/font/

# Installe le CSS généré après l'avoir rectifié
cat output/bow-icons.css | sed 's|url("./bow-icons|url("../font/bow-icons|g' > ../../../web/src/assets/less/_icons.less

sudo rm -rf output