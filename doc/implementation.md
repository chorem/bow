TODO: table d'historique d'authentification
TODO: faire des tests de perf entre TEXT[] et jsonb qui stockerait que des chaines
TODO: suggestion du add affiche le format (doc) et propose des bookmarks ayant la meme url
TODO: generation du opensearch.xml par le back avec generation d'un token jwt plus petit (seulement l'id) pour les suggesions qui ne recoivent pas les cookies

== Creation d'un compte

La création d'un compte, ne fait qu'envoyer un mail avec un lien contenant
un jeton jwt crypté ensuite avec "crypto/aes". Si la personne clique réellement
sur le lien cela valide la création du compte.

Le jeton contient:
- le mail
- le mot de passe

== Droit dans Postgresql

L'utilisateur ayant créé la base n'est pas utilisé pour le login applicatif

Il y a un utilisateur (nobody) qui n'a droit sur aucun objet met qui peut endoser le role
de tous les autres. C'est cet utilisateur qui est utilisé dans l'applicatif.
Si la personne qui arrive n'a pas de token (pas authentifiée) alors elle reste
avec cet utilisateur sans droit.

Les seuls droits qu'il a est:
- création d'un nouvelle utilisateur
- lecture des mots de passe

== Optimisation Posgresql

Les tableaux (ex: text[]) utilise des index gin, il faut donc utiliser seulement
les opérateurs de tableau pour faire les comparaisons dans les where, sinon
l'index n'est pas utilisé

Il faut remplacer `'mon@email' = ANY(emails)` par `emails @> '{"mon@email"}'::Text[]`

== Group

Il est possible dans les tags de mettre un `@` devant un tag pour indiquer un
groupe. Si ce groupe existe il donnera accès aux personnes ayant le droit de
lecture dans ce groupe.

Lorsqu'on recherche tous les tags `@toto` en tant qu'utilisateur, on a
forcément ses propres bookmarks qui ont le tag `@toto`, mais aussi tout ceux
des groupes auquels ont appartient et auquel le owner du bookmark appartient
aussi en tant que `adder`, `Writer` ou `admin` (mais pas `reader`).

Un groupe a des admins qui peuvent ajouter (retirer?) d'autres utilisateurs dans le
groupe. Par défaut le créateur est admin, mais il peut en ajouter d'autres.
Le groupe a des `writer` qui ont le droit d'ajouter ce même tags dans leurs bookmarks,
mais aussi de modifier les bookmarks avec ce tag, les `adder` ne peuvent qu'ajouter ce
tag, mais pas modifier les autres bookmarks qui ont ce tag
et des `reader` peuvent voir les bookmarks qui ont ce tag.

Des utilisateurs peuvent donc mettre le tag dans leurs bookmarks sans que 
personne ne les voit jusqu'au jour on il sont ajouter à un groupe qui porte
ce nom. Il peut y avoir plusieurs groupes avec le même nom, tout dépendra
de qui est dans le groupe.

les admins d'un groupe peuvent générer un token d'authentification pour un
groupe, ce qui permettra de diffuser une url avec ce token et ainsi permet
à n'importe qui de voir ces bookmarks

Un groupe est supprimé lorsqu'il n'y a plus d'utilisateur (admin, writer, reader)

Le owner du bookmark, les admin et writer d'un groupe peuvent supprimer le `tag`
du groupe dans un bookmark, ce qui fait disparaitre ce bookmark du groupe.

Lorsqu'on veut ajouter un utilisateur dans un groupe, l'ajout devra être validé
par l'utilisteur, sinon on pourrait lui "voler" des bookmarks en créant un groupe
avec un tag qu'il utilise et en l'ajoutant dans les `adder`
Pour ce faire une table `invitation` contient les invitations en cours. Une fois
validé, l'invitation est supprimée et on est ajouté au groupe dans la bonne 
catégorie (`admin`, `writer`, `adder`, `reader`)

| Droit | Invitation | Modification | Ajout | Lecture/recherche | 
| :---- | :--------: | :----------: | :---: | :---------------: |
| Admin |     X      |      X       |   X   |         X         |
| Writer|            |      X       |   X   |         X         |
| Adder |            |              |   X   |         X         |
| Reader|            |              |       |         X         |

- Invitation: permet de demander à quelque de venir dans le groupe
- Modification: permet de modifier les bookmarks qui ont se groupe comme tag
- Ajout: permet d'ajouter ce tag sur ces bookmarks pour les rendre visible pour ce groupe
- Lecture/recherche: permet de rechercher et lire les bookmarks qui ont ce groupe

```
CREATE TABLE invitation (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    creationDate TIMESTAMP  WITH TIME ZONE DEFAULT current_timestamp,
    validityDate TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
    initiator UUID NOT NULL, -- la personne qui a invité
    guest UUID NOT NULL,     -- la personne que est invité à rejoindre le groupe
    group UUID NOT NULL FOREIGN KEY ,     -- le groupe pour lequel l'invitation est faite
    role TEXT NOT NULL,      -- le role qu'aura l'invité dans le groupe s'il accepte
    message TEXT             -- un message que l
);

CREATE UNIQUE INDEX guest_group_unique ON invitation(guest,group); -- si on ajout une nouvelle, on remplace l'ancienne
CREATE INDEX invitation ON bowgroup (guest);
CREATE INDEX invitation ON bowgroup (group);

ALTER TABLE bowgroup ADD adder UUID[];
CREATE INDEX bowgroup_adder_idx ON bowgroup USING gin (adder);

GRANT UPDATE(adder) ON bowgroup TO person;
-- tout le monde peut lire les groupes si on est dans les utilisateurs du groupes
ALTER POLICY bowgroup_access_select ON bowgroup
    FOR SELECT
    USING ((admin || writer || adder || reader ) @> ('{' || current_user || '}')::uuid[]);

-- si un bookmark a comme tag '@toto' et proprietaire '1234' alors il est visible de toutes les utilisateurs 
-- appartenant (admin, writer, reader) au groupe 'toto' dont l'utilisateur '1234' est admin ou writer
ALTER POLICY bookmark_access_group ON bookmark
    FOR SELECT
    TO person
    USING ( current_user::uuid in (SELECT unnest(admin || writer || adder || reader) FROM bowgroup WHERE tags @> ('{@' || name || '}')::text[]
             AND ('{' || owner || '}')::uuid[] <@ (admin || writer || adder)));

GRANT SELECT, INSERT, DELETE ON invitation TO person;

ALTER TABLE invitation ENABLE ROW LEVEL SECURITY;
-- les admin d'un groupe peuvent ajouter/retirer des entrées pour ce groupe
CREATE POLICY invitation_insert ON invitation
    FOR INSERT
    WITH CHECK (initiator = current_user::uuid)
           ADD current_user::uuid in (SELECT unnest(admin) FROM bowgroup WHERE id = group);;

CREATE POLICY invitation_select ON invitation
    FOR SELECT, DELETE
    USING current_user::uuid in (SELECT unnest(admin) FROM bowgroup WHERE id = group);

-- les guests peuvent s'ajouter au groupe s'ils sont dans les invités puis se retirer des invitations
CREATE POLICY bowgroup_access_update ON bowgroup
    FOR UPDATE
    USING current_date < (SELECT validityDate FROM invitation i WHERE group = id AND guest = current_user::uuid)
    WITH CHECK current_user::uuid in (SELECT guest FROM invitation i WHERE group = id );

CREATE POLICY invitation_select ON invitation
    FOR SELECT, DELETE
    USING (guest = current_user::uuid);

```

Il faut qu'une personne puisse se retirer d'un group

== Les tokens de groupes

```
CREATE TABLE bowgroupToken (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    bowgroup UUID REFERENCES bowgroup ON DELETE CASCADE,
    creationDate TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
    updateDate TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,

    token TEXT NOT NULL UNIQUE, -- uuid
    description TEXT NOT NULL,
    expiration TIMESTAMP WITH TIME ZONE
);

CREATE INDEX bowgroupToken_bowgroup_idx ON bowgroupToken (bowgroup);
CREATE INDEX bowgroupToken_token_idx ON bowgroupToken (token);
CREATE TRIGGER update_bowgroupToken_updateDate BEFORE UPDATE ON bowgroupToken FOR EACH ROW EXECUTE PROCEDURE update_updateDate_column();

GRANT SELECT, INSERT, DELETE, TRIGGER ON bowgroupToken TO person;
GRANT UPDATE(updateDate, expiration) ON bowgroupToken TO person;

ALTER TABLE bowgroupToken ENABLE ROW LEVEL SECURITY;

```