authentication:
  call: 1199 
  min: 1.209914ms
  max: 60.005541ms
  avg: 8.45436ms
  StdDeviation: 8.257614ms

rest(GET /api/v1/bookmarks) # recherche par tag, ou fulltext
  call: 231
  min: 2.802321ms
  max: 278.448075ms
  avg: 28.181521ms
  StdDeviationHumain: 27.642803ms

rest(GET /api/v1/bookmarks/tags) # recherche des tags pour aide a la completion
  call: 114
  min: 35.417473ms
  max: 133.779361ms
  avg: 81.789094ms
  stddeviation: 18.547815ms

rest(GET /api/v1/bookmarks/{id}) # recherche d'un bookmark pour edition
  call: 11
  min: 2.570579ms
  max: 17.102177ms
  avg: 7.007831ms
  stddeviation: 4.660664ms

rest(GET /api/v1/bookmarks/{id}/visit) # increment du compteur de clique sur un lien et retourne le vrai lien
  call: 6
  min: 7.027394ms
  max: 12.335118ms
  avg: 10.369097ms
  stddeviation: 1.908622ms

rest(GET /api/v1/opensearch) # saisie dans la barre du navigateur (suggesion ou action)
  call: 731
  min: 68.143µs
  max: 330.865903ms
  avg: 41.814832ms
  stddeviation: 50.15556ms

rest(GET /api/v1/users/{id}) # recup d'un utilisateur pour modification des préférences
  call: 4
  min: 4.280183ms
  max: 17.300764ms
  avg: 8.182765ms
  stddeviation: 6.119834ms

rest(POST /api/v1/bookmarks) # creation d'un bookmark
  call: 90
  min: 6.69895ms
  max: 668.551172ms
  avg: 25.003866ms
  stddeviation: 73.254212ms

rest(PUT /api/v1/bookmarks/{id}) # modification d'un bookmark
  call: 11
  min: 6.3507ms
  max: 625.336368ms
  avg: 67.524142ms
  stddeviation: 185.030551ms
