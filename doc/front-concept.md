### Front

Le front est en vuejs + vuerouter + vuex

L'intégration de vuex n'est là que pour ce qui est vraiment commun à tous les composants:
- user pour les infos de l'utilisateur courant
- authenticated (boolean) pour savoir si l'utilisateur a un jeton jwt
- query qui contient les infos et le resultat de la requête courante (info: {}, tags:[[tags...],[occurence...]], result:)

Il faut sinon privilégier le dialogue parent/enfant dans les composants

Il exist un mixin déclarer dans main.js qui permet l'accès à toutes les
informations du store vuex sous forme de properties. Ce mixin apporte aussi un
ensemble de methodes pour faire évoluer la query courante.

Lorsque l'utilisateur se connecte ou se déconnecte, le user modifier est celui
du localStorage, qui envoie ensuite un event qui est intercepté dans App.js
pour mettre à jour le store vuex (user et authenticated)

La query elle, est mise à jour dans le store lorsqu'on recoie le nouveau
résultat, le composant qui fait cette requête lors du changement d'url est
Home.vue