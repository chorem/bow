-- migration des images de bytea en text base64

ALTER TABLE bowuser ADD lang TEXT DEFAULT 'en';
GRANT UPDATE (lang) ON bowUser TO person;

---- create above / drop below ----

REVOKE UPDATE (lang) ON bowUser FROM person;
ALTER TABLE bowuser DROP lang;
