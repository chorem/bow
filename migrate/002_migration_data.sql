CREATE OR REPLACE FUNCTION coalesce_agg_sfunc(state anyelement, value anyelement) RETURNS anyelement AS
$$
    SELECT coalesce(state, value);
$$ LANGUAGE SQL;

CREATE OR REPLACE AGGREGATE coalesce_agg(anyelement) (
    SFUNC = coalesce_agg_sfunc,
    STYPE  = anyelement);

CREATE OR REPLACE FUNCTION merge_array_sfunc(state anyarray, value anyarray) RETURNS anyarray AS
$$
    SELECT array( select distinct unnest(state || value));
$$ LANGUAGE SQL;

CREATE OR REPLACE AGGREGATE merge_array(anyarray) (
    SFUNC = merge_array_sfunc,
    STYPE  = anyarray);


-- to keep creation date
ALTER TABLE BowUser DISABLE TRIGGER update_BowUser_createDate;
ALTER TABLE Bookmark DISABLE TRIGGER update_Bookmark_createDate;

-- migration users
with
  -- all values field in single field
  w as (select id, fieldname, coalesce(numbervalue::text, datevalue::text, textvalue::text, booleanvalue::text, binaryvalue::text) as value from wikitty_data where id in (select id from wikitty_admin where extension_list like '%User%'))
  -- all action (aka bowSearchPrefix) in json with bad bey
, all_actions_badjson as (select jsonb_object_agg(fieldname, coalesce(numbervalue::text, datevalue::text, textvalue::text, booleanvalue::text, binaryvalue::text)) as __json from wikitty_data where fieldname like 'BowSearchPrefix.%' group by id)
  -- all action
, all_actions as (select __json->>'BowSearchPrefix.bowUser' as id, jsonb_agg(jsonb_build_object(
                   'prefix', __json->>'BowSearchPrefix.prefix',
                   'action', __json->>'BowSearchPrefix.search',
                   'suggest', __json->>'BowSearchPrefix.suggestion')
                 ) as actions from all_actions_badjson group by id)
  -- all authenticationInfo in json with bad key
, all_authenticationInfo_badjson as (select jsonb_object_agg(fieldname, coalesce(numbervalue::text, datevalue::text, textvalue::text, booleanvalue::text, binaryvalue::text)) as __json from wikitty_data where fieldname like 'BowAuthentication.%' group by id)
  -- all action
, all_authenticationInfo as (select __json->>'BowAuthentication.target' as id, jsonb_build_object(
                   'description', __json->>'BowAuthentication.description',
                   'login', __json->>'BowAuthentication.login',
                   'form', __json->>'BowAuthentication.form',
                   'domain', __json->>'BowAuthentication.domain',
                   'domaincomponent', 3,
                   'maxlength', (__json->>'BowAuthentication.maxLength')::smallint,
                   'allowedchar', __json->>'BowAuthentication.include',
                   'disallowedchar', __json->>'BowAuthentication.exclude',
                   'salt', __json->>'BowAuthentication.prefix',
                   'suffix', __json->>'BowAuthentication.suffix'
                   ) as authenticationInfo from all_authenticationInfo_badjson)
  -- convert all others field to json with bad key
, badjson as (select id, jsonb_build_object('id', id) || jsonb_object_agg(fieldname, value) as __json from w where fieldname not like 'Bookmark.tags[%' and fieldname not like 'WikittyLabel.labels[%' group by id)
  -- extract field from json to normalize name
, all_json as (select n.id, jsonb_build_object('id', n.id,
                   'creationdate', coalesce(__json->>'WikittyToken.date'::text, now()::text),
                   'login', __json->>'WikittyUser.login',
                   'password', __json->>'WikittyUser.password',
                   'tokens', json_build_array(jsonb_build_object('token', __json->>'BowUser.permanentToken', 'name', 'initial token')),
                   'autoscreenshot', __json->>'BowPreference.screenshot',
                   'autofavicon', __json->>'BowPreference.favicon',
                   'maxtagincloud', __json->>'BowPreference.tags',
                   'maxresult', __json->>'BowPreference.bookmarks',
                   'actions', actions,
                   'authenticationinfo', authenticationInfo) as __json
            from badjson n left join all_actions t on n.id=t.id left join all_authenticationInfo a on n.id=a.id)
-- insert bookmarks in new table
insert into bowUser select j.* from all_json, jsonb_populate_record(NULL::bowUser, __json) as j ON CONFLICT (login) DO NOTHING ;




-- migration bookmarks
with
  -- all values field in single field
  w as (select id, fieldname, coalesce(numbervalue::text, datevalue::text, textvalue::text, booleanvalue::text, binaryvalue::text) as value from wikitty_data where id in (select id from wikitty_admin where extension_list like '%Bookmark%'))
  -- convert tags/labels to text array
, tags as (select id, jsonb_agg(value) as tags from w where fieldname like 'Bookmark.tags[%' or fieldname like 'WikittyLabel.labels[%' group by id)
  -- all authenticationInfo in json with bad key
, all_authenticationInfo_badjson as (select jsonb_object_agg(fieldname, coalesce(numbervalue::text, datevalue::text, textvalue::text, booleanvalue::text, binaryvalue::text)) as __json from wikitty_data where fieldname like 'BowAuthentication.%' group by id)
  -- all action
, all_authenticationInfo as (select __json->>'BowAuthentication.target' as id, jsonb_build_object(
                   'description', __json->>'BowAuthentication.description',
                   'login', __json->>'BowAuthentication.login',
                   'form', __json->>'BowAuthentication.form',
                   'domain', __json->>'BowAuthentication.domain',
                   'domaincomponent', 3,
                   'maxlength', (__json->>'BowAuthentication.maxLength')::smallint,
                   'allowedchar', __json->>'BowAuthentication.include',
                   'disallowedchar', __json->>'BowAuthentication.exclude',
                   'salt', __json->>'BowAuthentication.prefix',
                   'suffix', __json->>'BowAuthentication.suffix'
                   ) as authenticationInfo from all_authenticationInfo_badjson)
  -- convert all others field to json
, nottags_json as (select id, jsonb_build_object('id', id) || jsonb_object_agg(fieldname, value) as __json from w where fieldname not like 'Bookmark.tags[%' and fieldname not like 'WikittyLabel.labels[%' group by id)
  -- extract field from json to normalize name
, all_json as (select n.id, jsonb_build_object('id', n.id,
                   'owner', coalesce( __json->>'BowBookmark.bowUser',__json->>'WikittyAuthorisation.owner', bowUser.id::text),
                   'uri', coalesce(__json->>'Bookmark.link', __json ->>'BowBookmark.link'),
                   'description', coalesce(__json->>'Bookmark.description', __json->>'BowBookmark.description'),
                   'creationdate', coalesce(__json->>'Bookmark.date', __json->>'WikittyToken.date', __json->>'BowBookmark.creationDate'),
                   'importdate', coalesce(__json->>'Import.date', __json->>'BowImport.importDate' ),
                   'privatealias', json_build_array(coalesce(__json->>'Bookmark.alias', __json->>'BowBookmark.privateAlias')),
                   'publicalias', json_build_array(__json->>'BowBookmark.publicAlias'),
                   'favicon', __json->>'BowBookmark.favicon',
                   'screenshot', __json->>'BowBookmark.screenshot',
                   'visit', coalesce(__json->>'Bookmark.click', __json->>'BowBookmark.click'),
                   'tags', tags,
                   'authenticationinfo', a.authenticationInfo,
                   'lang', 'english') as __json
            from nottags_json n left join tags t on n.id=t.id left join all_authenticationInfo a on n.id=a.id left join bowUser on __json->>'Bookmark.email'=bowUser.login)
, __row as (select j.* from all_json, jsonb_populate_record(NULL::bookmark, __json) as j inner join bowUser b on b.id=j.owner)
, __row_unique as (select
                   coalesce_agg(id) as id,
                   owner,
                   uri,
                   string_agg(description, '\n\n') as description,
                   merge_array(tags) as tags,
                   min(creationDate) as creationDate,
                   min(importDate) as importDate,
                   merge_array(privateAlias) as privateAlias,
                   merge_array(publicAlias) as publicAlias,
                   coalesce_agg(favicon) as favicon,
                   coalesce_agg(screenshot) as screenshot,
                   sum(visit) as visit,
                   coalesce_agg(authenticationInfo) as authenticationInfo,
                   coalesce_agg(lang) as lang
            from __row group by owner, uri)
-- insert bookmarks in new table
insert into bookmark (id, owner, uri, description, tags, creationdate, importdate, privatealias, publicalias, favicon, screenshot, visit, authenticationInfo, lang) select * from __row_unique;

update bowUser set creationDate = (select min(creationDate) from bookmark where owner=bowUser.id);

-- reactivate trigger
ALTER TABLE BowUser ENABLE TRIGGER update_BowUser_createDate;
ALTER TABLE Bookmark ENABLE TRIGGER update_Bookmark_createDate;

---- create above / drop below ----

DELETE FROM actionHistory;
DELETE FROM pageHistory;
DELETE FROM bookmark;
DELETE FROM bowgroup;
DELETE FROM bowUser;
