-- migration des images de bytea en text base64

ALTER TABLE bookmark ADD favicontext TEXT;
UPDATE bookmark SET favicontext=ENCODE(favicon, 'BASE64');
ALTER TABLE bookmark DROP favicon;
ALTER TABLE bookmark RENAME favicontext TO favicon;

ALTER TABLE bookmark ADD screenshottext TEXT;
UPDATE bookmark SET screenshottext=ENCODE(screenshot, 'BASE64');
ALTER TABLE bookmark DROP screenshot;
ALTER TABLE bookmark RENAME screenshottext TO screenshot;

---- create above / drop below ----

ALTER TABLE bookmark ADD faviconbytea bytea;
UPDATE bookmark SET faviconbytea=DECODE(favicon, 'BASE64');
ALTER TABLE bookmark DROP favicon;
ALTER TABLE bookmark RENAME faviconbytea TO favicon;

ALTER TABLE bookmark ADD screenshotbytea bytea;
UPDATE bookmark SET screenshotbytea=DECODE(screenshot, 'BASE64');
ALTER TABLE bookmark DROP screenshot;
ALTER TABLE bookmark RENAME screenshotbytea TO screenshot;

