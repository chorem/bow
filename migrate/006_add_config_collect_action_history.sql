-- Ajout de la configuration pour savoir si on doit sauver toutes les actions faites

ALTER TABLE bowuser ADD collectActionHistory BOOLEAN DEFAULT false;
GRANT UPDATE (collectActionHistory) ON bowUser TO person;

---- create above / drop below ----

ALTER TABLE bowuser DROP collectActionHistory;
