-- ATTENTION:
-- * il faut que bowUser.emails soit unique sur tous les utilisateurs
-- * il faut que bookmark.publicAlias soit unique sur tous les utilisateurs

CREATE EXTENSION IF NOT EXISTS "pgcrypto";
CREATE EXTENSION IF NOT EXISTS "pg_trgm";
CREATE EXTENSION IF NOT EXISTS "citext";

-- fonction a utilise pour les recherches dans les tableaux de text (ex: where text(tags) ilike '%adm%')
CREATE OR REPLACE FUNCTION text(text[])
  returns text language sql immutable as
  $$ select $1::text $$;

CREATE TYPE Token AS (
    name TEXT,
    token UUID,
    validity TIMESTAMP  WITH TIME ZONE
);

CREATE TYPE AuthenticationInfo AS (
    description text,
    login text,
    form text,
    domain text, -- force le domain (pour ne pas utiliser celui du site)
    domainComponent smallint, -- le nombre de composant du domain a prendre (default 2, ex: codelutin.com)
    maxLength smallint,
    allowedChar text,
    disallowedChar text,
    salt text,  -- chaine ajouter en prefix du master password avant le hash
    suffix text -- chaine ajouter en suffix du hash final genere (le password genere peut donc etre plus long que maxLength)
);

CREATE TYPE Action AS (
    prefix TEXT,
    action Text,
    suggest Text
);

CREATE TABLE bowUser (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    creationDate TIMESTAMP  WITH TIME ZONE DEFAULT current_timestamp,
    updateDate TIMESTAMP  WITH TIME ZONE DEFAULT current_timestamp,
    admin BOOLEAN DEFAULT false,
    login TEXT,
    password TEXT,
    tokens jsonb, -- [{name: sring, token: string, expire: date}]
    emails TEXT[],
    unconfirmedEmails jsonb, -- [{email: string, token: string, creationDate: date}]
    authenticationInfo jsonb, -- AuthenticationInfo,
    autoScreenshot BOOLEAN DEFAULT false,
    autoFavicon BOOLEAN DEFAULT false,
    maxTagInCloud SMALLINT DEFAULT 25,
    maxResult SMALLINT DEFAULT 25,
    actions jsonb -- [{"action": string, "prefix": string, "suggest": string}]
);

CREATE UNIQUE INDEX bowUser_login_idx ON BowUser (login);
CREATE INDEX bowUser_token_idx ON BowUser USING gin (tokens);
CREATE INDEX bowUser_emails_idx ON BowUser USING gin (emails); -- TODO garantir l'unicite avant l'insertion
CREATE INDEX bowUser_unconfirmedEmails_idx ON BowUser USING gin (unconfirmedEmails);

CREATE TABLE bowgroup (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    creationDate TIMESTAMP  WITH TIME ZONE DEFAULT current_timestamp,
    updateDate TIMESTAMP  WITH TIME ZONE DEFAULT current_timestamp,
    name TEXT,
    description TEXT,
    tokens jsonb, -- [{name: sring, token: string, expiration: date}]
    admin UUID[],
    writer UUID[],
    reader UUID[]
);

CREATE INDEX bowgroup_name_idx ON bowgroup (name);
CREATE INDEX bowgroup_token_idx ON bowgroup USING gin (tokens);
CREATE INDEX bowgroup_admin_idx ON bowgroup USING gin (admin);
CREATE INDEX bowgroup_writer_idx ON bowgroup USING gin (writer);
CREATE INDEX bowgroup_reader_idx ON bowgroup USING gin (reader);

CREATE TABLE bookmark (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    owner UUID REFERENCES bowUser(id) ON DELETE CASCADE ON UPDATE CASCADE,
    uri TEXT,
    description TEXT,
    tags TEXT[],
    creationDate TIMESTAMP  WITH TIME ZONE,
    updateDate TIMESTAMP  WITH TIME ZONE DEFAULT current_timestamp,
    importDate TIMESTAMP  WITH TIME ZONE,
    privateAlias TEXT[],
    publicAlias TEXT[],
    authenticationInfo jsonb, -- AuthenticationInfo,
    favicon BYTEA,
    screenshot BYTEA,
    visit BIGINT DEFAULT 0,
    lang regconfig NOT NULL DEFAULT 'english'::regconfig
);

CREATE INDEX bookmark_owner_idx ON bookmark(owner);
CREATE INDEX bookmark_tags_idx ON bookmark USING gin (tags);  -- index pour la recherche exact
CREATE INDEX bookmark_tags_like_idx ON bookmark USING gin (text(tags) gin_trgm_ops);  -- index pour la recherche via ilike "%toto%"
CREATE INDEX bookmark_description_idx ON bookmark USING GIN (to_tsvector(lang, description));
CREATE INDEX bookmark_privateAlias_idx ON bookmark USING gin (privateAlias);
CREATE INDEX bookmark_privateAlias_like_idx ON bookmark USING gin (text(privateAlias) gin_trgm_ops);  -- index pour la recherche via ilike "%toto%"
CREATE INDEX bookmark_publicAlias_idx ON bookmark USING gin (publicAlias); -- TODO garantir l'unicité
CREATE INDEX bookmark_all_ft_idx ON bookmark USING GIN(to_tsvector(lang, text(coalesce(tags, '{}'::text[])) || ' ' || coalesce(description, '') || ' ' || coalesce(uri, '') || ' ' || text(coalesce(privateAlias, '{}'::text[])) || ' ' || text(coalesce(publicAlias, '{}'::text[]))));


-- penser au nettoyage de cette table, si plus aucun bookmark ne reference cette uri
CREATE TABLE pageHistory (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    uri TEXT,
    creationDate TIMESTAMP  WITH TIME ZONE,
    content TEXT,
    lang regconfig NOT NULL DEFAULT 'english'::regconfig
);

CREATE INDEX pageHistory_uri_creationDate_idx ON pageHistory(uri, creationDate);
CREATE INDEX pageHistory_content_idx ON pageHistory USING GIN (to_tsvector(lang, content));

CREATE TABLE actionHistory (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    owner UUID REFERENCES bowUser(id) ON DELETE CASCADE ON UPDATE CASCADE,
    creationDate TIMESTAMP  WITH TIME ZONE,
    Action action,
    request TEXT,
    lang regconfig NOT NULL DEFAULT 'english'::regconfig
);

CREATE INDEX actionHistory_owner_idx ON actionHistory(owner);
CREATE INDEX actionHistory_request_idx ON actionHistory USING GIN (to_tsvector(lang, request));
CREATE INDEX actionHistory_action_idx ON actionHistory (((action).action));


-- Mise a jour automatique de la date de derniere modification
CREATE OR REPLACE FUNCTION update_creationDate_column()
RETURNS TRIGGER AS $$
BEGIN
    NEW.creationDate = now();
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE OR REPLACE FUNCTION update_updateDate_column()
RETURNS TRIGGER AS $$
BEGIN
    NEW.updateDate = now();
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_BowUser_createDate BEFORE INSERT ON BowUser FOR EACH ROW EXECUTE PROCEDURE update_creationDate_column();
CREATE TRIGGER update_bowgroup_createDate BEFORE INSERT ON bowgroup FOR EACH ROW EXECUTE PROCEDURE update_creationDate_column();
CREATE TRIGGER update_Bookmark_createDate BEFORE INSERT ON Bookmark FOR EACH ROW EXECUTE PROCEDURE update_creationDate_column();
CREATE TRIGGER update_PageHistory_createDate BEFORE INSERT ON pageHistory FOR EACH ROW EXECUTE PROCEDURE update_creationDate_column();
CREATE TRIGGER update_ActionHistory_createDate BEFORE INSERT ON actionHistory FOR EACH ROW EXECUTE PROCEDURE update_creationDate_column();

CREATE TRIGGER update_BowUser_updateDate BEFORE INSERT OR UPDATE ON BowUser FOR EACH ROW EXECUTE PROCEDURE update_updateDate_column();
CREATE TRIGGER update_bowgroup_updateDate BEFORE INSERT OR UPDATE ON bowgroup FOR EACH ROW EXECUTE PROCEDURE update_updateDate_column();
CREATE TRIGGER update_Bookmark_updateDate BEFORE INSERT OR UPDATE ON Bookmark FOR EACH ROW EXECUTE PROCEDURE update_updateDate_column();

-- -------------- S E C U R I T Y -------------- --

-- nobody n'herite pas des droits des autres pour le force a faire un "set role"
CREATE USER nobody WITH NOINHERIT CREATEROLE LOGIN PASSWORD '{{.nobody_password}}';

DROP ROLE IF EXISTS person;
CREATE ROLE person;

-- l'utilisateur nobody a le droit d'inserer des users (creation de compte) et c'est lui qui visite les pages
GRANT SELECT, INSERT ON bowUser TO nobody;
GRANT INSERT ON pageHistory TO nobody;
-- l'utilisateur nobody a le droit de lire les alias public et l'uri pour permettre la recheche et donc l'utilisation des alias public
GRANT SELECT(uri, publicAlias) ON bookmark TO nobody;

-- tout le monde a le droit de faire certaine chose, on restraint les updates au champs qui peuvent varier
GRANT SELECT, DELETE, TRIGGER ON bowUser TO person;
GRANT UPDATE (updateDate, password, tokens, emails, unconfirmedEmails, authenticationInfo, autoScreenshot, autoFavicon, maxTagInCloud, maxResult, actions) ON bowUser TO person;
GRANT SELECT, INSERT, DELETE, TRIGGER ON bowgroup TO person;
GRANT UPDATE(updateDate, description, tokens, admin, writer, reader) ON bowgroup TO person;
GRANT SELECT, INSERT, DELETE, TRIGGER ON bookmark TO person;
GRANT UPDATE(uri, description, tags, updateDate, privateAlias, publicAlias, authenticationInfo, favicon, screenshot, visit, lang) ON bookmark TO person;
-- on ne peut pas modifier une action, mais on peut la supprimer
GRANT SELECT, INSERT, DELETE, TRIGGER ON actionHistory TO person;
-- on ne peut que lire les historiques de pages
GRANT SELECT ON pageHistory TO person;

ALTER TABLE bowUser ENABLE ROW LEVEL SECURITY;
ALTER TABLE bowgroup ENABLE ROW LEVEL SECURITY;
ALTER TABLE bookmark ENABLE ROW LEVEL SECURITY;
ALTER TABLE actionHistory ENABLE ROW LEVEL SECURITY;

-- les utilisateurs n'ont le droit d'agir que sur leur propre compte
CREATE POLICY bowUser_access ON bowUser
    FOR ALL
    USING (id = current_user::uuid);

-- tout le monde peut lire les groupes si on est dans les utilisateurs du groupes
CREATE POLICY bowgroup_access_select ON bowgroup
    FOR SELECT
    USING ((admin || writer || reader ) @> ('{' || current_user || '}')::uuid[]);

-- tous les utilisateurs peuvent créer un groupe s'il finisse admin du groupe
CREATE POLICY bowgroup_access_insert ON bowgroup
    FOR INSERT
    WITH CHECK (admin @> ('{' || current_user || '}')::uuid[]);

-- seul les admins peuvent ajouter/retirer des utilisateurs d'un group, mais ils ne peuvent pas se retirer eux meme de l'admin (il reste toujours un admin)
CREATE POLICY bowgroup_access_update ON bowgroup
    FOR UPDATE
    USING (admin @> ('{' || current_user || '}')::uuid[]);

-- Seul le propriétaire peut tout faire sur son bookmark
CREATE POLICY bookmark_access ON bookmark
    FOR ALL
    USING (owner = current_user::uuid);

-- si un bookmark a comme tag '@toto' et proprietaire '1234' alors il est visible de toutes les utilisateurs 
-- appartenant (admin, writer, reader) au groupe 'toto' dont l'utilisateur '1234' est admin ou writer
CREATE POLICY bookmark_access_group ON bookmark
    FOR SELECT
    TO person
    USING ( current_user::uuid in (SELECT unnest(admin || writer || reader) FROM bowgroup WHERE tags @> ('{@' || name || '}')::text[]
             AND ('{' || owner || '}')::uuid[] <@ (admin || writer)));

-- tout le monde peut gerer des actionHistory, mais a la fin le createur doit etre owner (il n'y pas droit au update via le GRANT)
CREATE POLICY actionHistory_access ON actionHistory
    USING (owner = current_user::uuid);

-- droit specifique pour nobody
-- il peut lire tous les id, login, email, password
-- il peut creer de nouveau utilisateur
-- il peut faire des recherches sur les alias publics
CREATE POLICY bowUser_nobody_access ON bowUser
    FOR SELECT
    TO nobody
    USING (true);

CREATE POLICY bowUser_nobody_insert ON bowUser
    FOR INSERT
    TO nobody
    WITH CHECK (true);

-- pour lire les urls et les alias publics 
CREATE POLICY bookmark_nobody_access on bookmark
    FOR SELECT
    TO nobody
    USING (true);

-- creation automatique des roles lors de la creation d'un compte
CREATE OR REPLACE FUNCTION create_role()
RETURNS TRIGGER AS $$
DECLARE
    id varchar  := NEW."id";
BEGIN
    IF NOT EXISTS (SELECT * FROM pg_roles WHERE rolname = id) THEN
        -- creation du role pour l'utilisateur
        execute 'CREATE ROLE "' || id || '"';
        -- nobody peut prendre ce role
        execute 'GRANT "' || id || '" TO nobody';
        -- les users font parti du role person
        execute 'GRANT person TO "' || id || '"';
    END IF;

    RETURN NEW;
END;
$$ language 'plpgsql';

-- suppression automatique des roles lors de la suppression d'un compte
CREATE OR REPLACE FUNCTION delete_role()
RETURNS TRIGGER AS $$
DECLARE
    id varchar  := OLD."id";
BEGIN
    execute 'DROP ROLE IF EXISTS "' || id || '"';

    RETURN OLD;
END;
$$ language 'plpgsql';

CREATE TRIGGER create_role AFTER INSERT ON bowUser
    FOR EACH ROW EXECUTE PROCEDURE create_role();

CREATE TRIGGER delete_role AFTER DELETE ON bowUser
    FOR EACH ROW EXECUTE PROCEDURE delete_role();


---- create above / drop below ----

DROP TRIGGER create_role ON bowUser;
DROP TRIGGER delete_role ON bowUser;

DROP FUNCTION delete_role;
DROP FUNCTION create_role;

DROP POLICY bookmark_nobody_access ON bookmark;
DROP POLICY bowUser_nobody_insert ON bowUser;
DROP POLICY bowUser_nobody_access ON bowUser;
DROP POLICY actionHistory_access ON actionHistory;
DROP POLICY bookmark_access_group ON bookmark;
DROP POLICY bookmark_access ON bookmark;
DROP POLICY bowgroup_access_update ON bowgroup;
DROP POLICY bowgroup_access_insert ON bowgroup;
DROP POLICY bowgroup_access_select ON bowgroup;
DROP POLICY bowUser_access ON bowUser;

ALTER TABLE bowUser DISABLE ROW LEVEL SECURITY;
ALTER TABLE bowgroup DISABLE ROW LEVEL SECURITY;
ALTER TABLE bookmark DISABLE ROW LEVEL SECURITY;
ALTER TABLE actionHistory DISABLE ROW LEVEL SECURITY;

REVOKE INSERT ON bowUser FROM nobody;
REVOKE INSERT ON pageHistory FROM nobody;
REVOKE SELECT(uri, publicAlias) ON bookmark FROM nobody;

REVOKE SELECT, DELETE, TRIGGER ON bowUser FROM person;
REVOKE UPDATE (updateDate, password, tokens, emails, unconfirmedEmails, authenticationInfo, autoScreenshot, autoFavicon, maxTagInCloud, maxResult, actions) ON bowUser FROM person;
REVOKE SELECT, INSERT, DELETE, TRIGGER ON bowgroup FROM person;
REVOKE UPDATE(updateDate, description, tokens, admin, writer, reader) ON bowgroup FROM person;
REVOKE SELECT, INSERT, DELETE, TRIGGER ON bookmark FROM person;
REVOKE UPDATE(uri, description, tags, updateDate, privateAlias, publicAlias, authenticationInfo, favicon, screenshot, visit, lang) ON bookmark FROM person;
REVOKE SELECT, INSERT, DELETE, TRIGGER ON actionHistory FROM person;
REVOKE SELECT ON pageHistory FROM person;

DROP USER nobody;
DROP ROLE person;

DROP TABLE actionHistory;
DROP TABLE pageHistory;
DROP TABLE bookmark;
DROP TABLE bowgroup;
DROP TABLE bowUser;

DROP TYPE AuthenticationInfo;
DROP TYPE Action;
DROP TYPE Token;

DROP FUNCTION update_creationDate_column;
DROP FUNCTION update_updateDate_column;

DROP EXTENSION IF EXISTS "pgcrypto";
DROP EXTENSION IF EXISTS "pg_trgm";
