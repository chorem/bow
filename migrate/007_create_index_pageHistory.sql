-- creation d'un index pour retrouver le plus vite possible un historique le plus proche d'une date pour une url

CREATE EXTENSION IF NOT EXISTS "btree_gist";

CREATE INDEX pageHistory_url_createdate_idx ON pageHistory USING gist (creationdate, uri);

---- create above / drop below ----

DROP INDEX pageHistory_url_createdate_idx;
