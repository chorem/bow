-- on met les images dans une table a part pour pouvoir mutualise la place qu'elle prenne

-- penser au nettoyage de cette table, si plus aucun bookmark ne reference cette uri
CREATE TABLE pageInfo (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    uri TEXT UNIQUE,
    creationDate TIMESTAMP  WITH TIME ZONE DEFAULT current_timestamp,
    favicon TEXT,        -- base64 image 
    miniscreenshot TEXT, -- base64 image 65x91
    screenshot TEXT      -- base64 image 596x842
);

CREATE INDEX pageInfo_uri_idx ON pageInfo(uri);

-- securite
GRANT SELECT, INSERT, UPDATE, DELETE ON pageInfo TO nobody;
GRANT SELECT ON pageInfo TO person;

-- migration des données
INSERT INTO pageInfo (uri, favicon, screenshot) 
  SELECT uri, favicon, screenshot FROM bookmark
  WHERE favicon IS NOT NULL AND screenshot IS NOT NULL
  ON CONFLICT (uri) DO NOTHING;

INSERT INTO pageInfo (uri, favicon) 
  SELECT uri, favicon FROM bookmark
  WHERE favicon IS NOT NULL
  ON CONFLICT (uri) DO NOTHING;

INSERT INTO pageInfo (uri, screenshot) 
  SELECT uri, screenshot FROM bookmark
  WHERE screenshot IS NOT NULL
  ON CONFLICT (uri) DO NOTHING;

ALTER TABLE bookmark DROP screenshot;
ALTER TABLE bookmark DROP favicon;

---- create above / drop below ----

ALTER TABLE bookmark ADD screenshot TEXT;
ALTER TABLE bookmark ADD favicon TEXT;

UPDATE bookmark b SET favicon=i.favicon, screenshot=i.screenshot 
  FROM pageInfo i WHERE b.uri = i.uri;

DROP TABLE pageInfo;